#!/meta/h/habs/usr/bin/python3

import bs4

fn = 'errors_combined_uniq.txt'
errors = [ 'meets/{}_{}.html'.format(m.split(',')[1], m.split(',')[0]) for m in open(fn).read().split('\n') if ',' in m ]

#fn = 'real_errors.txt'
#errors = open(fn).read().split('\n')

for e in errors:
    with open(e) as f:
        ms = bs4.BeautifulSoup(f.read().replace('&nbsp;&nbsp;</a>', ''), 'html.parser')
        ats = ms.find_all('a', href = lambda x: x is not None and (x.startswith('http://www.tilastopaja.com/db/at.php?Sex=') or x.startswith('http://www.tilastopaja.eu/db/at.php?Sex=')), class_ = 'desktop')
        if len(ats) == 0:
            print('REAL REAL REAL REAL REAL REAL REAL REAL REAL {}'.format(e))
        else:
            print('{} {}'.format(len(ats), e))
