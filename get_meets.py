#!/usr/pkg/bin/python3

import os
import bs4
import csv
import tqdm
import time
import glob
import sqlite3
import requests
import datetime
import urllib.parse as urlparse

from stem import Signal
from stem.control import Controller

import tb2_private

USE_CACHE = False

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate(password=tb2_private.torpw)
        controller.signal(Signal.NEWNYM)

finnish_months = {
    'tammikuu': 'January',
    'helmikuu': 'February',
    'maaliskuu': 'March',
    'huhtikuu': 'April',
    'toukokuu': 'May',
    'kesäkuu': 'June',
    'heinäkuu': 'July',
    'elokuu': 'August',
    'syyskuu': 'September',
    'lokakuu': 'October',
    'marraskuu': 'November',
    'joulukuu': 'December',

    # oddballs
    'Marc': 'March',
    'Septem': 'September',
    'Novem': 'November',
    'Dece': 'December',
}
stt = '%m/%d/%Y'

#r = requests.post('http://www.tilastopaja.com/db/resultservice.php', data = {'Search': '\' or \'a\'=\'a'}, cookies = tb2_private.tilastopaja_cookies, stream = True)
#if not os.path.isfile('meets.html'):
#    with open('meets.html', 'wb') as f:
#        for data in tqdm.tqdm(r.iter_content()):
#            f.write(data)
#writefile = 'meets.csv'
#readfile = ['meets.html', 'monaco.html'][0]
#with open(readfile, 'r') as f:
#    rs = bs4.BeautifulSoup(f.read(), 'html.parser')

#if not os.path.isfile('meets.html'):
#    with open('meets.html', 'wb') as f:
#        for data in tqdm.tqdm(r.iter_content()):
#            f.write(data)

con = sqlite3.connect('file:/meta/h/habs/trackbot/tbfilter.sqlite?mode=ro', uri = True)
cur = con.cursor()
print('getting all t_cids...')
cur.execute('select distinct t_cid from tbfilter')
allmeets = [ str(c[0]) for c in cur.fetchall() ]
print('{} meets in db'.format(len(allmeets)))
cur.close()

if not USE_CACHE:
    renew_connection()
    session = get_tor_session()
    timestamp = str(int(time.time()))
    r = session.get('https://www.tilastopaja.eu/db/resultservice.php', cookies = tb2_private.tilastopaja_cookies, stream = True)
    with open('newmeets/meets_{}.html'.format(timestamp), 'wb') as f:
        for data in tqdm.tqdm(r.iter_content()):
            f.write(data)
    rs = bs4.BeautifulSoup(open('newmeets/meets_{}.html'.format(timestamp)).read(), 'html.parser')
else:
    utimes = [ int(gf.split('_')[1].split('.')[0]) for gf in glob.glob('newmeets/meets_*.html') ]
    utimes.sort(reverse = True)
    newtime = utimes[0]
    rs = bs4.BeautifulSoup(open('newmeets/meets_{}.html'.format(utimes[0])).read(), 'html.parser')

writefile = 'newmeets/newmeets_{}.csv'.format(int(time.time()))
trstart = 0
trs = rs.find_all('table')[-1].find_all('tr')
for i, tr in enumerate(trs[trstart:]):
    print('[ %d / %d ]' % (i+trstart, len(trs)))
    if len(tr.find_all('td')) <= 1:
        continue
    meet = {}
    tds = tr.find_all('td')
    upq = urlparse.parse_qs(urlparse.urlparse(tds[1].a['href']).query)
    year = upq['Season'][0]
    meet['start_year'] = year
    meet['tilas_cid'] = upq['CID'][0]
    if meet['tilas_cid'] in allmeets:
        print('{} exists in db! skipping...'.format(meet['tilas_cid']))
        continue
    meet['tilas_shortname'] = tds[0].text
    meet['country'] = tds[2].text
    if meet['country'] == 'USA' and tds[1].text != '':
        meet['city'] = ' '.join(tds[1].text.split()[:-1])
        meet['state'] = tds[1].text.split()[-1]
    else:
        meet['city'] = tds[1].text
        meet['state'] = ''
    if '-' not in tds[3].text:
        if tds[3].text == '':
            meet['start_date'] = ''
            meet['end_date'] = ''
        else:
            day, month = tds[3].text.split()
            if month in finnish_months:
                month = finnish_months[month]
            sed = ' '.join([day, month, year])
            sedt = datetime.datetime.strptime(sed, '%d %B %Y')
            meet['start_date'] = sedt.strftime(stt)
            meet['end_date'] = sedt.strftime(stt)
    else:
        defmonth = tds[3].text.split()[-1]
        if defmonth in finnish_months:
            defmonth = finnish_months[defmonth]
        sd = [ dt.strip() for dt in tds[3].text.split('-') ][0]
        if sd.isdigit():
            sd = sd + ' ' + defmonth + ' ' + year
        else:
            day, month = sd.split()
            if month in finnish_months:
                month = finnish_months[month]
            sd = ' '.join([day, month, year])
        sdt = datetime.datetime.strptime(sd, '%d %B %Y')
        meet['start_date'] = sdt.strftime(stt)
        ed = [ dt.strip() for dt in tds[3].text.split('-') ][-1]
        day, month = ed.split()
        if month in finnish_months:
            month = finnish_months[month]
        ed = ' '.join([ day, month, year ])
        edt = datetime.datetime.strptime(ed, '%d %B %Y')
        while sdt > edt:
            ed = [ dt.strip() for dt in tds[3].text.split('-') ][-1] + ' ' + str(int(year)+1)
            edt = datetime.datetime.strptime(ed, '%d %B %Y')
        meet['end_date'] = edt.strftime(stt)
    meet['tilas_type'] = tds[4].text
    meet['tilas_longname'] = tds[5].text
    with open(writefile, 'a', newline = '') as out:
        writer = csv.writer(out)
        writer.writerow([ meet[abe] for abe in [ 'tilas_cid', 'start_year', 'tilas_shortname', 'city', 'state', 'country', 'start_date', 'end_date', 'tilas_type', 'tilas_longname' ] ])
