#!/usr/pkg/bin/python3

import csv

tfmeets = {}
with open('tfmeets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        mtup = (row[6], row[4])
        if mtup in tfmeets:
            tfmeets[mtup].append(row)
        else:
            tfmeets[mtup] = [ row ]

collisions = 0
with open('../meets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        if row[6] is not None and row[4] is not None:
            mtup = (row[6], row[4])
            if mtup in tfmeets:
                collisions += 1
                for omt in tfmeets[mtup]:
                    print('{:20} {:20} {}'.format(row[9], row[2], omt[9]))

print(collisions)
