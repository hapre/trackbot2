#!/usr/pkg/bin/python3

import bs4
import csv
import time
import glob
import timeit
import sqlite3
import requests
import datetime

from stem import Signal
from stem.control import Controller

import tb2_private

USE_CACHE = True

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate(password = tb2_private.torpw)
        controller.signal(Signal.NEWNYM)

dnow = datetime.datetime.now()

nowyear = dnow.strftime('%Y')
nowmon = dnow.strftime('%-m')

#writefile = 'tfmeetdir/{}_{}.html'.format(int(time.time()))

headers = {
    'Host': 'www.tfrrs.org',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://www.tfrrs.org/results_search.html',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
}

def get_meets():
    sess = get_tor_session()
    latest = (dnow.year * 12) + dnow.month - 1
    earliest = (2008 * 12) + 0
    for i, dt in enumerate(range(latest, earliest, -1)):
        ot = timeit.default_timer()
        year = dt // 12
        month = dt % 12 + 1
        outfn = 'tfmeetdir/{}_{}.html'.format(year, str(month).zfill(2))
        print('[{} / {}] {}'.format(i, latest - earliest, outfn))
        data = [
            ('meet_name', ''),
            ('sport', 'track:xc'),
            ('state', ''),
            ('month', str(month)),
            ('year', str(year)),
        ]
        sess = get_tor_session()
        srs = sess.post('https://www.tfrrs.org/results_search.html', headers = headers, data = data)
        if srs.status_code != 200 or len(srs.text) < 350:
            print(srs.text)
        with open(outfn, 'w') as f:
            f.write(srs.text)
        print(timeit.default_timer() - ot)

if not USE_CACHE:
    get_meets()

states = {
    'AL': 'Alabama',
    'AK': 'Alaska',
    'AB': 'Alberta',
    'AZ': 'Arizona',
    'AR': 'Arkansas',
    'BC': 'British Columbia',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DE': 'Delaware',
    'DC': 'District of Columbia',
    'FL': 'Florida',
    'GA': 'Georgia',
    'HI': 'Hawaii',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'IA': 'Iowa',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'ME': 'Maine',
    'MB': 'Manitoba',
    'MD': 'Maryland',
    'MA': 'Massachusetts',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MS': 'Mississippi',
    'MO': 'Missouri',
    'MT': 'Montana',
    'NE': 'Nebraska',
    'NV': 'Nevada',
    'NB': 'New Brunswick',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NY': 'New York',
    'NF': 'Newfoundland',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'NT': 'Northwest Territories',
    'NS': 'Nova Scotia',
    'NU': 'Nunavut Territory',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'ON': 'Ontario',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PE': 'Prince Edward Island',
    'PR': 'Puerto Rico',
    'QC': 'Quebec',
    'RI': 'Rhode Island',
    'SK': 'Saskatchewan',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'VI': 'U.S. V.I.',
    'UT': 'Utah',
    'VT': 'Vermont',
    'VA': 'Virginia',
    'WA': 'Washington',
    'WV': 'West Virginia',
    'WI': 'Wisconsin',
    'WY': 'Wyoming',
    'YT': 'Yukon Territory',

    'UK': 'United Kingdom',
}

notusa = {
    'BC': 'CAN',
    'MB': 'CAN',
    'NB': 'CAN',
    'NF': 'CAN',
    'NT': 'CAN',
    'NS': 'CAN',
    'NU': 'CAN',
    'PE': 'CAN',
    'QC': 'CAN',
    'SK': 'CAN',
    'YT': 'CAN',

    'UK': 'GBR',
}

csvhead = [ 'tilas_cid', 'start_year', 'tilas_shortname', 'city', 'state', 'country', 'start_date', 'end_date', 'tilas_type', 'tilas_longname' ]
# tilas_type: Indoor, Cross country, none if outdoor
def parse_meets():
    meetfns = sorted(glob.glob('tfmeetdir/*.html'), reverse = True)
    for i, fn in enumerate(meetfns):
        print('[{} / {}] {}'.format(i, len(meetfns), fn))
        fnsoup = bs4.BeautifulSoup(open(fn).read(), 'html.parser')
        pgs = fnsoup.find_all('tbody', { 'id': lambda x: x.startswith('results_page') })
        for pg in pgs:
            for meettr in pg.find_all('tr'):
                print('.', end = '')
                abe = {}
                tds = meettr.find_all('td')
                datestr = tds[0].text.strip()
                enddate = datetime.datetime.strptime(datestr.split(' - ')[-1], '%m/%d/%y')
                abe['end_date'] = enddate.strftime('%m/%d/%Y')
                if ' - ' in tds[0].text:
                    startdate = datetime.datetime.strptime(datestr.split(' - ')[0], '%m/%d').replace(year = enddate.year)
                    abe['start_date'] = startdate.strftime('%m/%d/%Y')
                else:
                    abe['start_date'] = abe['end_date']
                abe['start_year'] = abe['start_date'][-4:]
                abe['tilas_longname'] = tds[1].text.strip()
                abe['tilas_cid'] = tds[1].find('a')['href'].split('/')[-1].replace('.html', '').strip()
                stabbr = tds[-1].text.strip()
                abe['country'] = 'USA' if stabbr not in notusa else notusa[stabbr]
                abe['state'] = stabbr
                abe['city'] = states[stabbr] if stabbr in states else '' # TODO put actual city
                typeabbr = tds[2].text.strip()
                abe['tilas_type'] = 'Cross country' if typeabbr == 'XC' else '' # TODO diff between outdoor / indoor
                abe['tilas_shortname'] = '' # TODO eh
                with open('tfmeets.csv', 'a', newline = '') as out:
                    csv.writer(out).writerow([ abe[hd] for hd in csvhead ])
        print()

parse_meets()
        
