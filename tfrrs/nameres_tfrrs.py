#!/usr/pkg/bin/python3

import io
import re
import bs4
import csv
import glob
import uuid
import timeit
import datetime

csvhead = [ 'tilas_cid', 'start_year', 'tilas_shortname', 'city', 'state', 'country', 'start_date', 'end_date', 'tilas_type', 'tilas_longname' ]

fns = sorted(glob.glob('tfres/*.html'))
splitno = 1000 # was 5000 before the first one
RESUME = 19139 # 22202 # 22000 # 33627 # 18346 # 25000 # 5000 # 911
END = 22000 # 25000
if RESUME == -1:
    RESUME += 1
    for i in range(0, len(fns), splitno):
        open('nameres_tfrrs/nrtf_{}.csv'.format(str(i // splitno).zfill(3)), 'w').close()

meets = {}
with open('tfmeets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets[row[0]] = row

def append(cols, dum):
    ruuid = str(uuid.uuid4())
    cols = cols + [ ruuid ]
    if cols[10+7] == '1': # 10 == number of meetdeets cols
        print(cols)
    csv.writer(dum, quoting = csv.QUOTE_NONNUMERIC).writerow(cols)

cfields = [ 'fname', 'lname', 'evgender', 'evname', 'meet', 'wind', 'anchor', 'place', 'tfidslug', 'grade', 'team', 'result', 'resultstring', 'teamslug', 'score', 'squad', 'meet_type', 'avgmile', 'realcity' ]

def parse(fns):
    sumwaits = 0
    noparsed = 0
    for i, fn in enumerate(fns):
        if i < RESUME or i >= END:
            continue
        ot = timeit.default_timer()
        print('[ {} / {} ] {} ({} noparsed)'.format(i, len(fns), fn, noparsed))
        dum = io.StringIO()
        meetid = fn.split('/')[-1].split('_')[0]
        ms = bs4.BeautifulSoup(open(fn, encoding = 'latin-1').read().replace('<A name="', '</A><A name="'), 'html.parser')
        realcitysoup = ms.find_all('div', class_ = 'panel-heading-normal-text inline-block ') # trailing space intentional
        realcity = realcitysoup[-1].text.split(' - ')[-1].split(', ')[0].strip().split('\n')[-1].strip() if realcitysoup else None
        meetdeets = meets[meetid]
        if 'xc' in fn:
            meet = ms.find('h3', class_ = 'panel-title').text.strip()
            meet_type = 'Cross country'
            for tbl in ms.find_all('table'):
                title = tbl.find_previous('h3', class_ = 'font-weight-500')
                if title is None or 'Team Results' in title.text:
                    continue # TODO check if i'm missing anything by this nonecheck
                title = ' '.join([ sep for sep in title.text.split() ])
                evname = title[title.rfind('(') + 1:title.rfind(')')].strip() if ')' in title else None # recently fixed to only get last parens
                gender = 'Women' if 'Women' in title else 'Men'
                for atres in tbl.find_all('tr'):
                    if atres.find('th'): # header (should only and always be first)
                        tdk = { th.text.strip(): i for i, th in enumerate(atres.find_all('th')) }
                        continue
                    if 'NAME' not in tdk:
                        continue # hack for weird team scores, we always want names in xc
                    tds = atres.find_all('td')
                    tdtxs = [ td.text.strip() for td in tds ]
                    abe = {}
                    abe['meet'] = meet
                    abe['meet_type'] = meet_type
                    abe['evgender'] = gender
                    abe['evname'] = evname
                    abe['wind'] = None
                    abe['anchor'] = None
                    abe['squad'] = None
                    abe['realcity'] = realcity
                    abe['place'] = tdtxs[tdk['PL']] if 'PL' in tdk and tdtxs[tdk['PL']] != '' else None
                    namepair = tdtxs[tdk['NAME']].split(',')
                    abe['fname'] = namepair[1].strip()
                    abe['lname'] = namepair[0].strip()
                    abe['tfidslug'] = tds[tdk['NAME']].find('a')['href'] if tds[tdk['NAME']].find('a') else None
                    abe['grade'] = tdtxs[tdk['YEAR']] if 'YEAR' in tdk else None
                    abe['avgmile'] = tdtxs[tdk['Avg. Mile']] if 'Avg. Mile' in tdk else None
                    abe['result'] = tdtxs[tdk['TIME']] if 'TIME' in tdk else None # TODO numerize it in csv
                    abe['resultstring'] = tdtxs[tdk['TIME']] if 'TIME' in tdk else None
                    abe['score'] = tdtxs[tdk['SC']] if 'SC' in tdk else None
                    abe['team'] = tdtxs[tdk['TEAM']] if 'TEAM' in tdk else None
                    abe['teamslug'] = tds[tdk['TEAM']].find('a')['href'] if 'TEAM' in tdk and tds[tdk['TEAM']].find('a') else None
                    append(meetdeets + [ abe[hd] for hd in cfields ], dum)
        else:
            gender = 'Men' if 'm' in fn.split('_')[-1].replace('.html', '', 1) else 'Women'
            meet = ms.find('h3', class_ = 'panel-title').text.strip()
            for ancg in ms.find_all('a', { 'name': True }):
                # an 'anchor group' is a group of just one 'real' final, OR one 'final by time' and the heats (duplicated) that went into it
                finals = ancg.find_all('h3', class_ = 'font-weight-500 pl-5')
                subfinals = [ h5 for h5 in ancg.find_all('h5') if 'Top' not in h5.text ]
                anchor = ancg['name']
                if len(finals) > 1:
                    print(finals)
                    print('ERROR len(finals) > 1')
                    exit()
                # only get h5s (subfinals) unless there are no h5s, in that case just get the h3 (to fix prelim / final results being duplicated)
                if len(subfinals) > 0:
                    recordheats = [ ( h5.text.strip(), h5.parent.parent.parent ) for h5 in subfinals ]
                else:
                    recordheats = [ ( finals[0].text.strip(), finals[0].parent.parent.parent ) ]
                for rectup in recordheats:
                    evname = ' '.join([ sep.strip() for sep in rectup[0].split() ])
                    rec = rectup[1]
                    for atres in rec.find_all('tr'):
                        if atres.find('li'): # field event details row
                            continue
                        if atres.find('th'): # header row (should always be first and only one of them), get td key
                            tdk = { th.text.strip(): i for i, th in enumerate(atres.find_all('th')) }
                            evdiv = atres.parent.parent.find_previous('div')

                            wtsoup = evdiv.find('span', class_ = 'wind-text')
                            if wtsoup:
                                wind = wtsoup.find('nobr').text.strip()
                            else:
                                wind = None
                            continue
                        tds = atres.find_all('td')
                        tdtxs = [ td.text.strip() for td in tds ]
                        abe = {}
                        abe['meet_type'] = None
                        abe['avgmile'] = None
                        abe['evgender'] = gender
                        abe['evname'] = evname
                        abe['meet'] = meet
                        abe['wind'] = wind
                        abe['anchor'] = anchor
                        abe['realcity'] = realcity
                        abe['place'] = tdtxs[tdk['PL']][:-1] if '.' in tdtxs[tdk['PL']] else None
                        if 'NAME' in tdk and ', ' in tdtxs[tdk['NAME']]:
                            namepair = tdtxs[tdk['NAME']].split(',')
                            abe['fname'] = namepair[1].strip()
                            abe['lname'] = namepair[0].strip()
                        else:
                            abe['fname'] = None
                            abe['lname'] = None
                        abe['tfidslug'] = tds[tdk['NAME']].find('a')['href'] if 'NAME' in tdk and tds[tdk['NAME']].find('a') else None
                        abe['grade'] = tdtxs[tdk['YEAR']] if 'YEAR' in tdk else None
                        if 'METRIC' in tdk:
                            abe['result'] = tdtxs[tdk['METRIC']].replace('m', '') if 'm' in tdtxs[tdk['METRIC']] else None
                            abe['resultstring'] = tdtxs[tdk['METRIC']]
                        elif 'MARK' in tdk:
                            abe['result'] = tdtxs[tdk['MARK']].replace('m', '') if 'm' in tdtxs[tdk['MARK']] else None
                            abe['resultstring'] = tdtxs[tdk['MARK']]
                        elif 'TIME' in tdk:
                            abe['result'] = tdtxs[tdk['TIME']] # TODO fix from csv
                            abe['resultstring'] = tdtxs[tdk['TIME']]
                        elif 'POINTS' in tdk:
                            abe['result'] = tdtxs[tdk['POINTS']] # TODO make sure to check for multi / field events and not convert to time
                            abe['resultstring'] = tdtxs[tdk['POINTS']]
                        abe['team'] = tdtxs[tdk['TEAM']] if 'TEAM' in tdk else None
                        abe['teamslug'] = tds[tdk['TEAM']].find('a')['href'] if 'TEAM' in tdk and tds[tdk['TEAM']].find('a') else None
                        abe['score'] = tdtxs[tdk['SC']] if 'SC' in tdk else None
                        if 'SQUAD' in tdk:
                            abe['squad'] = tdtxs[tdk['SQUAD']]
                        else:
                            abe['squad'] = None
                        if 'ATHLETES' in tdk: # should always be last
                            for relayatl in tds[tdk['ATHLETES']].find_all('a'):
                                spntag = relayatl.find('span')
                                if spntag:
                                    abe['fname'] = relayatl.find('span').text.strip()
                                    abe['lname'] = relayatl.text.replace(abe['fname'], '', 1).strip()
                                else:
                                    abe['lname'] = relayatl.text.strip()
                                    abe['fname'] = None
                                abe['tfidslug'] = relayatl['href']
                                append(meetdeets + [ abe[hd] for hd in cfields ], dum)
                            abe['fname'] = None
                            abe['lname'] = None
                            abe['tfidslug'] = None
                        append(meetdeets + [ abe[hd] for hd in cfields ], dum)
        with open('nameres_tfrrs/nrtf_{}.csv'.format(str(i // splitno).zfill(3)), 'a') as out:
            out.write(dum.getvalue())
            out.flush()
        wait = timeit.default_timer() - ot
        sumwaits += wait
        noparsed += 1
        print('{} (avg {})'.format(wait, sumwaits / noparsed))

parse(fns)
#parse([
#    'tfres/51935_m.html', # men's ncaa outdoors 2017
#    'tfres/51935_f.html', # women's ncaa outdoors 2017
#     'tfres/13423_xc.html', # ncaa xc 2017
#     'tfres/3885_xc.html', # Ventura Invitational xc
#])
