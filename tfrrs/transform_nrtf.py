#!/usr/pkg/bin/python3

import re
import csv
import glob
import uuid
import timeit
import sqlite3
import hashlib
import datetime

# note for realcity, check if state first, then do realcity.split('\n')[-1]
# fix things like '8k) Individual Results (8k' in evname
# fix trailing semicolon off team name
# open csv respecting newlines for broken realcitys
# replace T&F; with T&F in titles
# what to do about gender :-/ (broken up to 19139, 22000-22202, and broken 2500-end)
# check for '_f_' in team? but then what if unattached person wins
# check for unattached male runners in non-XC meets

csvhead = [ 'tilas_cid', 'start_year', 'tilas_shortname', 'city', 'state', 'country', 'start_date', 'end_date', 'tilas_type', 'tilas_longname' ]
cfields = [ 'fname', 'lname', 'evgender', 'evname', 'meet', 'wind', 'anchor', 'place', 'tfidslug', 'grade', 'team', 'result', 'resultstring', 'teamslug', 'score', 'squad', 'meet_type', 'avgmile', 'realcity' ]
allfields = csvhead + cfields

SPEC = '1.3.1' # change along with transform_nrt and func in tbf

speclist = [ 'year', 'series', 'meet', 'city', 'country', 'meet_start', 'meet_end', 'meet_type', 'fname', 'lname', 'nationality', 'birthdate', 'age', 'gender', 'event', 'evgender', 'relay', 'round', 'heat', 'result', 'resultstring', 'dnf', 'dq', 'wind', 'place', 'hand_timed', 'qualification', 't_cid', 't_aid', 't_eid', 'state', 'score', 'team', 'teamslug', 'grade', 'meet_uuid', 'result_uuid' ] # NOTE ADDED SCORE AND TEAM AND GRADE

con = sqlite3.connect('tbfilter_tfrrs.sqlite')
cur = con.cursor()

DROP = True
if DROP:
    cur.execute('drop table if exists tbfilter;')
    con.commit()

cur.execute('create table if not exists tbfilter ({});'.format(', '.join(speclist)))

cur.execute('select count(*) from tbfilter')
print('{} entries in db'.format(cur.fetchall()[0][0]))

utimes = [ int(gf.split('_')[1].split('.')[0]) for gf in glob.glob('newmeets/nrtfnew_*.csv') ]
utimes.sort(reverse = True)
newtime = utimes[0] if len(utimes) > 0 else None

UPDATE = False
newms = [ 'newmeets/nrtnew_{}.csv'.format(newtime) ]

def evfixer(sev):
    sev = re.sub('k$', 'km', sev) # dangerous if events end in 'k'
    sev = re.sub(' Mile$', ' miles', sev)
    sev = re.sub(' Finals$', '', sev)
    sev = re.sub(' Meters$', 'm', sev)
    sev = re.sub(' Hurdles$', 'mH', sev)
    sev = re.sub(' x ', 'x', sev)
    sev = re.sub('^Men\'s ', '', sev)
    sev = re.sub('^Women\'s ', '', sev)
    sev = re.sub(' Relay', '', sev)
    sev = re.sub(' Steeplechase', 'm Steeplechase', sev)

    sev = re.sub(' Division [0-9]', '', sev)
    sev = re.sub(' Collegiate/Open', '', sev)
    sev = re.sub(' Championship', '', sev)
    sev = re.sub(' College/Open', '', sev)
    sev = re.sub(' College/Univ', '', sev)
    sev = re.sub(' High School', '', sev)
    sev = re.sub(' University', '', sev)
    sev = re.sub(' College', '', sev)
    sev = re.sub(' Open', '', sev)
    # TODO something with masters / midget
    return sev

months = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
]
monthgex = re.compile('|'.join(months))

def get(res, cri):
    ret = res[allfields.index(cri)]
    if ret is None or ret.strip() == '':
        return None
    return ret

ot = timeit.default_timer()
for fn in [ 'newmeets/nrtfnew_{}.csv'.format(newtime) ] if newtime else [] + sorted(list(glob.glob('nameres_tfrrs/nrtf_*.csv'))):
    print(fn)
    t = timeit.default_timer()
    with open(fn, 'r', newline = '') as f:
        dr = csv.reader(f)
        todbs = []
        for r in dr:
            todb = {}
            todb['year'] = int(get(r, 'start_year'))
            todb['series'] = None
            todb['meet'] = get(r, 'meet')
            realcity = get(r, 'realcity')
            if realcity:
                todb['city'] = get(r, 'city') if monthgex.match(realcity) else realcity
            else:
                todb['city'] = get(r, 'city')
            todb['country'] = get(r, 'country')
            todb['meet_start'] = datetime.datetime.strptime(get(r, 'start_date'), '%m/%d/%Y') if get(r, 'start_date') else None
            todb['meet_end'] = datetime.datetime.strptime(get(r, 'end_date'), '%m/%d/%Y') if get(r, 'end_date') else None
            todb['meet_type'] = get(r, 'tilas_type')
            todb['fname'] = get(r, 'fname')
            todb['lname'] = get(r, 'lname')
            todb['nationality'] = None
            todb['birthdate'] = None
            todb['age'] = None
            # precedence: xc evgender > teamslug gender > event title gender > no gender
            if todb['meet_type'] == 'Cross country':
                todb['evgender'] = get(r, 'evgender')
            elif get(r, 'teamslug') and '_m_' in get(r, 'teamslug'):
                todb['evgender'] = 'Men'
            elif get(r, 'teamslug') and '_f_' in get(r, 'teamslug'):
                todb['evgender'] = 'Women'
            elif get(r, 'evname') and 'Men' in get(r, 'evname'):
                todb['evgender'] = 'Men'
            elif get(r, 'evname') and 'Women' in get(r, 'evname'):
                todb['evgender'] = 'Women'
            else:
                todb['evgender'] = '?'
            todb['gender'] = 'Male' if todb['evgender'] == 'Men' else 'Female' if todb['evgender'] == 'Women' else '?'
            todb['event'] = get(r, 'evname')
            if ' Section' in todb['event']:
                todb['heat'] = todb['event'][todb['event'].find('Section'):]
                todb['event'] = todb['event'][:todb['event'].find(' Section')]
            elif ' Heat' in todb['event']:
                todb['heat'] = todb['event'][todb['event'].find('Heat'):]
                todb['event'] = todb['event'][:todb['event'].find(' Heat')]
            elif ' Flight' in todb['event']:
                todb['heat'] = todb['event'][todb['event'].find('Flight'):]
                todb['event'] = todb['event'][:todb['event'].find(' Flight')]
            else:
                todb['heat'] = None
            if ' Preliminaries' in todb['event']:
                todb['round'] = 'Preliminary Round'
                todb['event'] = todb['event'][:todb['event'].find(' Preliminaries')]
            else:
                todb['round'] = None
            todb['event'] = evfixer(todb['event'])
            todb['relay'] = get(r, 'squad')
            todb['resultstring'] = get(r, 'resultstring')
            if get(r, 'resultstring'):
                todb['dnf'] = 'DNS' if 'DNS' in get(r, 'resultstring') else 'DNF' if 'DNF' in get(r, 'resultstring') else None
                todb['dq'] = get(r, 'resultstring') if 'DQ' in get(r, 'resultstring') else None
            else:
                todb['dnf'] = 'DNF'
                todb['dq'] = None
            if get(r, 'result') is not None:
                resw = get(r, 'result').split()[0]
                if (':' in resw or '.' in resw) and (not todb['dnf']) and (not todb['dq']):
                    result = float(''.join( c for c in resw.split(':')[-1] if c.isdigit() or c == '.' ))
                    if resw.count(':') == 2:
                        result += 60 * float(resw.split(':')[-2])
                        result += 3600 * float(resw.split(':')[-3])
                    elif resw.count(':') == 1:
                        result += 60 * float(resw.split(':')[-2])
                    todb['result'] = result
                else:
                    todb['result'] = None
            else:
                todb['result'] = None
            try:
                todb['wind'] = int(get(r, 'wind').replace('+', ''))
            except:
                todb['wind'] = None
            try:
                todb['place'] = int(get(r, 'place'))
            except:
                todb['place'] = None
            todb['hand_timed'] = None
            todb['qualification'] = None
            todb['t_cid'] = 't' + get(r, 'tilas_cid')
            t_aid = get(r, 'tfidslug')
            if t_aid:
                t_aid = t_aid[t_aid.find('athletes'):]
                t_aid = t_aid.split('/')[1].strip()
                t_aid = t_aid.replace('.html', '').strip()
                todb['t_aid'] = 't' + t_aid
            else:
                todb['t_aid'] = None
            todb['t_eid'] = ('t' + get(r, 'anchor')) if get(r, 'anchor') else None
            todb['state'] = get(r, 'state')
            try:
                todb['score'] = get(r, 'score')
            except:
                todb['score'] = None
            todb['grade'] = get(r, 'grade')
            todb['team'] = get(r, 'team')
            teamslug = get(r, 'teamslug')
            if teamslug and '/xc/' in teamslug:
                teamslug = teamslug[teamslug.find('/xc/'):].split('/')[2]
                todb['teamslug'] = teamslug.replace('.html', '').strip()
            elif teamslug and 'teams/' in teamslug:
                teamslug = teamslug[teamslug.find('teams/'):].split('/')[1]
                todb['teamslug'] = teamslug.replace('.html', '').strip()
            else:
                todb['teamslug'] = None
            todb['meet_uuid'] = str(uuid.UUID(hashlib.sha1((get(r, 'meet') + get(r, 'start_date')).encode('utf-8')).hexdigest()[:32]))
            todb['result_uuid'] = r[-1]
            #for key in allfields:
            #    print('{}: {}'.format(key, r[allfields.index(key)]))
            todbs.append([ todb[sp] for sp in speclist ])
            #print(todbs[-1])
            
    cur.executemany('insert into tbfilter ({}) VALUES ({});'.format(', '.join(speclist), ', '.join(['?'] * len(speclist))), todbs)
    con.commit()
    print(timeit.default_timer() - t)

print(timeit.default_timer() - ot)
con.close()
