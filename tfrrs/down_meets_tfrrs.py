#!/usr/pkg/bin/python3

import csv
import time
import glob
import timeit
import requests
import datetime

from stem import Signal
from stem.control import Controller

import tb2_private

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate(password = tb2_private.torpw)
        controller.signal(Signal.NEWNYM)

headers = {
    'Host': 'www.tfrrs.org',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://www.tfrrs.org/results_search.html',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
}

meets = []
with open('tfmeets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

def down_meets(start = 0):
    dls = 0
    sumwaits = 0
    sess = get_tor_session()
    for i, m in enumerate(meets[start:]):
        if i % 50 == 0:
            sess = get_tor_session()
            renew_connection()
        ot = timeit.default_timer()
        if m[8] == 'Cross country':
            gens = [ 'xc' ]
        else:
            gens = [ 'm', 'f' ]
        for gen in gens:
            fn = 'tfres/{}_{}.html'.format(m[0], gen)
            url = 'https://www.tfrrs.org/results/xc/{}.html'.format(m[0]) if gen == 'xc' else 'https://www.tfrrs.org/results/{}_{}.html'.format(m[0], gen)
            r = None
            attempts = 0
            while r is None:
                attempts += 1
                if attempts >= 10:
                    print('error: giving up on {}'.format(fn))
                    open('down_meets_errors.txt', 'a').write('{}\n'.format(fn))
                    r = None
                    break
                print('[ {} / {} ] ({} dls) {}'.format(i + start, len(meets), dls, fn))
                try:
                    r = sess.get(url.format(m[0], gen), headers = headers, timeout = 30)
                except Exception as e:
                    print(e)
                    r = None
                    session = get_tor_session()
                    renew_connection()
                if r is None or r.status_code != 200:
                    r = None
                    session = get_tor_session()
                    renew_connection()
            if r is not None:
                with open(fn, 'wb') as f:
                    f.write(r.content)
        dls += 1
        wait = timeit.default_timer() - ot
        sumwaits += wait
        print('{} (avg {})'.format(wait, sumwaits / dls))

down_meets(start = 2509)
