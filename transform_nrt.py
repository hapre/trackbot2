#!/meta/h/habs/usr/bin/python3

import re
import csv
import glob
import uuid
import timeit
import sqlite3
import hashlib
import datetime

# TODO: change round, heat to be None if empty

SPEC = '1.5.0' # remember to change it (1.3.1: fixed dqs?) (1.4.1: t_cid int -> str) (1.5.0: added score, team, teamslug, grade for tfrrs compat)
CURRENT_YEAR = 2018 # year of linux on the desktop

con = sqlite3.connect('tbfilter.sqlite')
cur = con.cursor()

DROP = True
if DROP:
    cur.execute('drop table if exists tbfilter;')
    con.commit()

speclist = [ 'year', 'series', 'meet', 'city', 'country', 'meet_start', 'meet_end', 'meet_type', 'fname', 'lname', 'nationality', 'birthdate', 'age', 'gender', 'event', 'evgender', 'relay', 'round', 'heat', 'result', 'resultstring', 'dnf', 'dq', 'wind', 'place', 'hand_timed', 'qualification', 't_cid', 't_aid', 't_eid', 'state', 'score', 'team', 'teamslug', 'grade', 'meet_uuid', 'result_uuid' ] # NOTE ADDED SCORE AND TEAM AND TEAMSLUG AND GRADE

cur.execute('create table if not exists tbfilter ({});'.format(', '.join(speclist)))

cur.execute('select count(*) from tbfilter')
print('{} entries in db'.format(cur.fetchall()[0][0]))

utimes = [ int(gf.split('_')[1].split('.')[0]) for gf in glob.glob('newmeets/nrtnew_*.csv') ]
utimes.sort(reverse = True)
newtime = utimes[0]

UPDATE = False
newms = [ 'newmeets/nrtnew_{}.csv'.format(newtime) ]

def evfixer(sev):
    sev = re.sub(' m$', 'm', sev)
    sev = re.sub(' km$', 'km', sev)
    sev = re.sub(' y$', 'y', sev)
    sev = re.sub('^One Mile$', 'Mile', sev)
    sev = re.sub('^Two Miles$', '2 Mile', sev)
    sev = re.sub('jump$', 'Jump', sev)
    sev = re.sub('vault$', 'Vault', sev)
    sev = re.sub(' m steeple$', 'm Steeplechase', sev)
    sev = re.sub(' m [Hh]urdles$', 'mH', sev)
    sev = re.sub('^Shot$', 'Shot Put', sev)
    sev = re.sub('^Hammer Throw$', 'Hammer', sev)
    sev = re.sub('^Discus Throw$', 'Discus', sev)
    sev = re.sub('^Javelin Throw$', 'Javelin', sev)
    sev = re.sub(' x ', 'x', sev)
    sev = re.sub('^Sprintmedley$', 'Sprint Medley', sev)
    sev = re.sub('^Distancemedley$', 'Distance Medley', sev)
    return sev

ot = timeit.default_timer()
for fn in sorted(list(glob.glob('newmeets/nrtnew_*.csv')), reverse = True) + list(glob.glob('nameres_tilas/nrt_*.csv')):
    print(fn)
    t = timeit.default_timer()
    with open(fn, 'r') as f:
        dr = csv.reader(f)
        todb = []
        for r in dr:
            year = int(r[1])
            series = r[2]
            meet = r[9]
            city = r[3]
            state = r[4]
            country = r[5]
            meet_start = datetime.datetime.strptime(r[6], '%m/%d/%Y') if r[6] != '' else None
            if meet_start is not None and meet_start.year > CURRENT_YEAR:
                meet_start = meet_start.replace(year = meet_start.year - 100)
            meet_end = datetime.datetime.strptime(r[7], '%m/%d/%Y') if r[7] != '' else None
            if meet_end is not None and meet_end.year > CURRENT_YEAR:
                meet_end = meet_end.replace(year = meet_end.year - 100)
            meet_type = r[8]
            fname = r[10]
            lname = r[11]
            nationality = r[12]
            if len(r[13]) > 0 and r[13].split()[-1].isdigit():
                rspl = r[13].split()
                if len(rspl) == 3 and rspl[0] == '0': r[13] = '1 ' + rspl[1] + ' ' + rspl[2]
                fmt = { 1: '%y', 2: '%b %y', 3: '%d %b %y' }[len(rspl)]
                birthdate = datetime.datetime.strptime(r[13], fmt)
                if birthdate.year > CURRENT_YEAR - 5:
                    birthdate = birthdate.replace(year = birthdate.year - 100)
            else:
                birthdate = None
            if birthdate is not None and meet_start is not None:
                age = meet_start.year - birthdate.year - ((meet_start.month, birthdate.day) < (meet_start.month, birthdate.day))
            else:
                age = None
            if r[14] == '1':
                gender = 'Male'
            elif r[14] == '2':
                gender = 'Female'
            else:
                gender = '?'
            t_eid = r[18]
            if t_eid.startswith('1'):
                evgender = 'Men'
            elif t_eid.startswith('2'):
                evgender = 'Women'
            else:
                evgender = '?'
            event = evfixer(r[17])
            if 'x' in event or 'medley' in event.lower():
                relay = True
            else:
                relay = None
            round = r[19]
            heat = r[20]
            if 'DQ' in r[21]:
                dq = r[21][r[21].index('DQ'):]
            else:
                dq = None
            resw = r[21].split()[0] if r[21] != '' else None
            resultstring = resw
            if (':' in resw or '.' in resw) and 'DNF' not in resw and 'DQ' not in resw and dq is not None:
                result = float(''.join( c for c in resw.split(':')[-1] if c.isdigit() or c == '.' ))
                if resw.count(':') == 2:
                    result += 60 * float(resw.split(':')[-2])
                    result += 3600 * float(resw.split(':')[-3])
                elif resw.count(':') == 1:
                    result += 60 * float(resw.split(':')[-2])
            else:
                result = None # None = DNF or DQ
            if 'DNF' in r[21]:
                dnf = True
            else:
                dnf = None
            if 'h' in r[21]:
                hand_timed = True
            else:
                hand_timed = None
            try:
                wind = float(r[22])
            except ValueError:
                wind = None
            try:
                place = int(r[23])
            except:
                place = None
            qualification = r[24]
            t_cid = r[0] # used to be an int
            meet_uuid = str(uuid.UUID(hashlib.sha1(r[0].encode('utf-8')).hexdigest()[:32]))
            try:
                t_aid = int(r[15])
            except ValueError:
                t_aid = None
            result_uuid = r[-1]
            onespeclist = ( year, series, meet, city, country, meet_start, meet_end, meet_type, fname, lname, nationality, birthdate, age, gender, event, evgender, relay, round, heat, result, resultstring, dnf, dq, wind, place, hand_timed, qualification, t_cid, t_aid, t_eid, state, None, None, None, None, meet_uuid, result_uuid )
            todb.append(onespeclist)
    cur.executemany('insert into tbfilter ({}) VALUES ({});'.format(', '.join(speclist), ', '.join(['?'] * len(speclist))), todb)
    con.commit()
    print(timeit.default_timer() - t)

print(timeit.default_timer() - ot)
con.close()
