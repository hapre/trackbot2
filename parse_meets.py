#!/meta/h/habs/usr/bin/python3

import bs4
import csv
import sys
import timeit
import pickle
import urllib.parse as urlparse

sys.setrecursionlimit(500000)

meets = []
with open('meets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

mfields = 10
fields = [
    # start meet
    'tilas_meet_cid',
    'tilas_meet_year',
    'tilas_meet_shortname',
    'meet_city',
    'meet_state',
    'meet_country',
    'meet_start_date',
    'meet_end_date',
    'tilas_meet_type',
    'tilas_meet_longname',
    # end meet (see mfields)
    'gender',
    'classification', # see paralympics
    'event',
    'tilas_eventid',
    'round',
    'heatno',
    'date',
    'place',
    'first_name',
    'last_name', # separated by &nbsp;
    'relay1_name',
    'relay2_name',
    'relay3_name',
    'relay4_name',
    'tilas_sex', # number
    'tilas_id',
    'country',
    'dob',
    'result', # DNS / DNF here
    'qualification',
    'wind',
    'tilas_quals',
    'tilas_records',
    'reaction_time',
    'multi_scorecard',
    'tilas_dopetext',
    'tilas_dopelength',
]

for i in range(30):
    for nom in [ 'mark', 'status', 'wind', 'tilas_quals' ]:
        fields.append('field{}_{}'.format(i+1, nom))

open('athdb.csv', 'w').close()

t = timeit.default_timer()
for i, m in enumerate(meets, start = 1):
    m[0] = '12877460'; m[1] = '2016'
    print('[ {} / {} ]'.format(i, len(meets)))
    with open('meets/{}_{}.html'.format(m[1], m[0]), 'r') as f:
        #ms = bs4.BeautifulSoup(f.read(), 'html5lib')
        #with open('rio.pkl', 'wb') as f: pickle.dump(ms, f, -1); exit()
        with open('rio.pkl', 'rb') as f: ms = pickle.load(f)
        ferow = False
        multirow = False
        evd = {}
        trs = ms.find('table', border = '0').find_all('tr')
        for trno, tr in enumerate(trs):
            tds = tr.find_all('td')
            if multirow:
                print('multirow ignored!')
                multirow = False
                continue
            if trno < len(trs)-1 and trs[trno+1].find_all('td')[1].find('a') is None and 'event' in evd and evd['event'] in ['Decathlon']:
                multirow = True
            if ferow:
                ferow = False
                continue
            if tr.find('td', class_ = 'sex') is not None:
                continue
            dt = tr.find('td', class_ = 'date')
            if dt is not None:
                ev = tr.find('td', class_ = 'event')
                if ev is not None:
                    evd['event'] = ev.b.text.strip()
                    evd['tilas_eventid'] = ev['id'].split('.')[1]
                    evd['round'] = 'Final'
                date = tr.find('td', class_ = 'date', colspan = '3')
                if date is not None:
                    evd['date'] = date.text.strip()
                rnd = tr.find('td', class_ = 'round')
                if rnd is not None:
                    evd['round'] = rnd.b.text.strip()
                    evd['heatno'] = '1'
                heat_list = [ t.text for t in tr.contents if 'Heat ' in t.text ]
                if len(heat_list) > 0:
                    evd['heatno'] = heat_list[0].strip().split()[-1]
                wind_list = [ t.text for t in tr.contents if 'Wind' in t.text ]
                if len(wind_list) > 0:
                    evd['wind'] = wind_list[0].strip().split()[-1]
                continue
            a = { k : '' for k in fields }
            for j in range(mfields):
                a[fields[j]] = m[j]
            for k in evd.keys():
                a[k] = evd[k]
            a['place'] = tds[0].text.strip()
            upq = urlparse.parse_qs(urlparse.urlparse(tds[1].find('a')['href']).query)
            a['tilas_sex'] = upq['Sex'][0]
            a['tilas_id'] = upq['ID'][0]
            name = tds[1].find('a').text.split('\xa0')
            a['first_name'] = name[0]
            a['last_name'] = name[1]
            a['country'] = tds[3].text.strip()
            a['dob'] = tds[4].text.strip()
            a['result'] = tds[5].text.strip()
            a['tilas_quals'] = ','.join([ ah.text.strip() for ah in tds[8].find_all('a') if ah.text not in ['«','»'] ])
#            print(evd)
#            print(tds[8])
            if ' x ' in a['event']:
                evupq = { 'Event': [ evd['event'] ] }
            else:
                evupq = urlparse.parse_qs(urlparse.urlparse(tds[8].find('a')['onclick'].split('"')[1]).query)
            if evupq['Event'][0] in ['PV', 'HJ'] and a['result'] != 'DNS':
                ferow = True
                attd = trs[trno+1].find('td', colspan = '7')
                atno = 1
                for at in attd.text.split():
                    if '/' in at:
                        if 'X' in at or 'R' in at:
                            for _ in range(at.count('X')):
                                a['field{}_mark'.format(atno)] = at.split('/')[0]
                                a['field{}_status'.format(atno)] = 'X'
                                atno += 1
                            continue
                        for _ in range(int(at.split('/')[1])-1):
                            a['field{}_mark'.format(atno)] = at.split('/')[0]
                            a['field{}_status'.format(atno)] = 'X'
                            atno += 1
                        a['field{}_mark'.format(atno)] = at.split('/')[0]
                        a['field{}_status'.format(atno)] = 'O'
                        atno += 1
            elif evupq['Event'][0] in [ 'LJ', 'TJ' ] and a['result'] != 'DNS':
                ferow = True
            elif evupq['Event'][0] in [ 'SP', 'DT', 'HT', 'JT' ] and a['result'] != 'DNS':
                ferow = True
            elif evupq['Event'][0] in [ '4 x 100 m', '4 x 400 m' ]:
                relayrow = True
            with open('athdb.csv', 'a') as out:
                orow = [ a[f] for f in fields ]
                print({ k: v for k, v in a.items() if v })
                print(','.join(orow))
                csv.writer(out).writerow(orow)

print(timeit.default_timer() - t)
