#!/usr/pkg/bin/python3

import io
import bs4
import csv
import sys
import glob
import uuid
import time
import timeit
import pickle
import urllib.parse as urlparse

import tb2_private

sys.setrecursionlimit(500000)

# DONE (for relays): fix spaces in front of names
# DONE: fix \xa0 names
# DONE (rebuild): fix pre-12000 country codes
# DONE: switch to full birthday
# DONE: get prelim / final number
# DONE: get qualification status
# DONE: report meets wih no athlete links (need to redownload)
# DONE: assign each result a UUID
# TODO: do new sqli meetlist, download only new meets, update meetlist
# DONE: fix LJ, TJ, PV results set to wind e.g. '1.4' or just 'w' accidentally
# DONE: add wind numbers (check sprints, LJ, TJ, PV)
# DONE: double check http://habs.sdf.org/tb2/new/meets/2005_8729997.html
# DONE: split output file every 10k meets to not slow everything down
# DONE: add multi event support
# DONE: add original country
# DONE (rebuild): before m#17751, fix names ending in &nbsp;
# TODO: fix numbering progress / resume (via dates rather than meetno?)
# DONE (rebuild): pre-15716 fix high school events being labeled as multi events
# TODO: created directory of cached pickled soupfiles to speed up rebuilds?
# TODO: check overlap #50000, also #47958
# TODO: check errors.txt, maybe new pattern to grep for? <table>\r\n</table> after 'doping ban' but check for false positives
# TODO: fix gendered relays??

SPEC = 1.0 # Change this any time output format changes

#meets = []
#with open('meets.csv', newline = '') as f:
#    reader = csv.reader(f)
#    for row in reader:
#        meets.append(row)
#print('have meets')

timestamp = str(int(time.time()))

meets = []
utimes = [ int(gf.split('_')[1].split('.')[0]) for gf in glob.glob('newmeets/newmeets_*.csv') ]
utimes.sort(reverse = True)
newtime = utimes[0]
with open('newmeets/newmeets_{}.csv'.format(utimes[0]), newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

# MEET FORMAT: [ 'tilas_cid', 'start_year', 'tilas_shortname', 'city', 'state', 'country', 'start_date', 'end_date', 'tilas_type', 'tilas_longname' ]


splitno = 10000

RESUME = 1 # one-indexed and inclusive
END = 50000 # one-indexed and inclusive
#if RESUME == 1:
#    for i in range(0, len(meets), splitno):
#        open('nameres_tilas/nrt_{}.csv'.format(str(i // splitno).zfill(3)), 'w').close()

def append(cols):
    ruuid = str(uuid.uuid4())
    cols = cols + [ ruuid ]
    csv.writer(dum, quoting = csv.QUOTE_NONNUMERIC).writerow(cols)
    if cols[-4] == '1': # TODO must edit every time i change the cols to point to place
        print('{} {}'.format(status, ','.join([ str(c) for c in cols ])))

t = timeit.default_timer()
for i, m in enumerate(meets[RESUME - 1:END], start = RESUME + 1):
    status = '[{} / {}]'.format(i, len(meets))
    dum = io.StringIO()
    #m[1] = '2017'
    #m[0] = '12898233'
    with open('meets/{}_{}.html'.format(m[1], m[0]), 'r') as f:
        ms = bs4.BeautifulSoup(f.read().replace('&nbsp;&nbsp;</a>', ''), 'html.parser')
        ats = ms.find_all('a', href = lambda x: x is not None and (x.startswith('http://www.tilastopaja.com/db/at.php?Sex=') or x.startswith('http://www.tilastopaja.eu/db/at.php?Sex=')), class_ = 'desktop')
        if len(ats) == 0:
            with open('errors.txt', 'a') as err:
                print('ERROR! {}'.format(m))
                csv.writer(err).writerow(m)
            continue
        for at in ats:
            tr = at.parent.parent
            birthday = tr.find_all('td')[4].text
            cnt = tr.find_all('td')[3].text
            wind = ''
            reses = tr.find_all(lambda x: x.name == 'td' and 'right' in x.get('align', []) and not 'desktop' in x.get('class', []))
            res = reses[0].text.strip()
            if reses[1].text.strip() != '': wind = reses[1].text.strip()
            place = tr.find('td').text
            sround = ''
            sheat = ''
            etr = tr
            while etr.find('td', { 'class': 'event', 'id': True }) is None:
                etr = etr.find_previous('tr')
                if len(etr.find_all('td')) >= 2:
                    etrtd = etr.find_all('td')[1]
                    if 'round' in etrtd.get('class', []) or 'date' in etrtd.get('class', []):
                        if wind == '' and 'Wind:' in etr.find_all('td')[-1].text:
                            wind = etr.find_all('td')[-1].text.replace('Wind: ', '').strip()
                        if sround == '' and 'round' in etrtd['class']:
                            sround = etrtd.find('b').text
                        if sheat == '' and 'date' in etrtd['class']:
                            sheat = etrtd.find('b').text
            if wind == '' and 'Wind:' in etr.find_all('td')[-1].text and sheat == '' and sround == '':
                wind = etr.find_all('td')[-1].text.replace('Wind: ', '').strip()
            event = etr.find('b').text
            evid = etr.find_all('td')[1]['id']
            multi = ''
            if etr.find_all('td')[1]['id'].split('.')[-1] != '-999':
                multi = 'multi'
            qual = ''
            if tr.find_all('td')[-2].text in [ 'Q', 'q' ]:
                qual = tr.find_all('td')[-2].text
            if res.strip() == '': continue # error
            if len(at.text.strip()) == 0: # relay?
                upq = urlparse.parse_qs(urlparse.urlparse(at['href']).query) # nation URL
                country = at.parent.parent.find_all('td')[3].text
                orow = m + [ country, country, cnt, 'country', upq['Sex'][0], upq['ID'][0], multi, event, evid, sround, sheat, res, wind, place, qual, i ]
                append(orow)
                rnamestr = at.parent.parent.find_next('tr')
                rnamestd = None
                if rnamestr is not None: rnamestd = rnamestr.find('td', colspan = '8')
                if rnamestd is not None:
                    for an in rnamestd.text.strip().split(', '):
                        if an.strip() == '': continue
                        orow = m + [ an.split()[0], ' '.join(an.split()[1:] if len(an.split()) >= 2 else ''), cnt, 'relay', 'relay', 'relay', multi, event, evid, sround, sheat, res, wind, place, qual, i ]
                        append(orow)
                continue
            upq = urlparse.parse_qs(urlparse.urlparse(at['href']).query)
            orow = m + [ at.text.split('\xa0')[0], at.text.split('\xa0')[1], cnt, birthday, upq['Sex'][0], upq['ID'][0], multi, event, evid, sround, sheat, res, wind, place, qual, i ]
            append(orow)
    #exit()
    with open('newmeets/nrtnew_{}.csv'.format(timestamp), 'a') as out:
        out.write(dum.getvalue())
        out.flush()
#    with open('nameres_tilas/nrt_{}.csv'.format(str(i // splitno).zfill(3)), 'a') as out:
#        out.write(dum.getvalue())
#        out.flush()

print(timeit.default_timer() - t)
