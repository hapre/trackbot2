#!/meta/h/habs/usr/bin/python3

import os
import csv
import timeit
import requests
import itertools
import tb2_private
from stem import Signal
import urllib.parse as urlparse
from stem.control import Controller

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate(password=tb2_private.torpass)
        controller.signal(Signal.NEWNYM)

meets = []
with open('meets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

sumwaits = 0.0
headers = requests.utils.default_headers()
session = get_tor_session()
start = 88965

print("session.get('http://www.tilastopaja.com/db/results.php', params = { 'CID': '12877460', 'Season': '2016' }, cookies = tb2_private.tilastopaja_cookies, timeout = 60)")
