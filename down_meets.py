#!/usr/pkg/bin/python3

import os
import bs4
import csv
import glob
import timeit
import requests
import itertools
import tb2_private
from stem import Signal
import urllib.parse as urlparse
from stem.control import Controller

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port = 9051) as controller:
        controller.authenticate(password=tb2_private.torpw)
        controller.signal(Signal.NEWNYM)

#meets = []
#with open('meets.csv', newline = '') as f:
#    reader = csv.reader(f)
#    for row in reader:
#        meets.append(row)

meets = []
utimes = [ int(gf.split('_')[1].split('.')[0]) for gf in glob.glob('newmeets/newmeets_*.csv') ]
utimes.sort(reverse = True)
newtime = utimes[0]
with open('newmeets/newmeets_{}.csv'.format(utimes[0]), newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

sumwaits = 0.0
skipped = 0

headers = {
    'Host': 'www.tilastopaja.eu',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Cache-Control': 'max-age=0',
}

brokens = [] #open('real_errors.txt').read().split('\n')
fixeds = [] #open('down_meets_fixed.txt').read().split('\n')
dls = 0

session = get_tor_session()
start = 0 #123975 #88964
for i, m in enumerate(meets[start:]):
    fn = 'meets/{}_{}.html'.format(m[1], m[0])
    if os.path.isfile(fn):
        print('Skipped {} ({}_{}.html)'.format(i, m[1], m[0]))
        skipped += 1
        continue
    dls += 1
    t = timeit.default_timer()
    r = None
    attempts = 0
    while r is None or 'Server Error' in r.text or 'Currently logged out' in r.text:
        attempts += 1
        if attempts >= 10:
            print('error: giving up on {}'.format(m))
            open('down_meets_errors.txt', 'a').write('{}\n'.format(m))
            continue
        print('[ {} / {} ] ({} dls) {}_{}.html'.format(start+i, len(meets)-1, dls, m[1], m[0]))
        try:
            r = session.get('https://www.tilastopaja.eu/db/results.php', params = { 'CID': m[0], 'Season': m[1] }, cookies = tb2_private.tilastopaja_cookies, headers = headers, timeout = 60)
        except Exception as e:
            print(e)
            r = None
            session = get_tor_session()
    ms = bs4.BeautifulSoup(r.text.replace('&nbsp;&nbsp;</a>', ''), 'html.parser')
    ats = ms.find_all('a', href = lambda x: x is not None and (x.startswith('http://www.tilastopaja.com/db/at.php?Sex=') or x.startswith('http://www.tilastopaja.eu/db/at.php?Sex=')), class_ = 'desktop')
    if len(ats) == 0:
        print('no results here :-(')
        open('down_meets_empties.txt', 'a').write('{}\n'.format(m))
    session = get_tor_session()
    with open(fn, 'wb') as f:
        f.write(r.content)
    renew_connection()
    session = get_tor_session()
    with open('prog.txt', 'w') as f:
        f.write('{}'.format(i+start))
    wait = timeit.default_timer() - t
    sumwaits += wait
    print('{} (avg {})'.format(wait, sumwaits / ((i - skipped) + 1)))
