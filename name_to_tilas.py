#!/meta/h/habs/usr/bin/python3

import bs4
import csv
import sys
import timeit
import pickle
import urllib.parse as urlparse

sys.setrecursionlimit(500000)

meets = []
with open('meets.csv', newline = '') as f:
    reader = csv.reader(f)
    for row in reader:
        meets.append(row)

#open('name_to_tilas.csv', 'w').close()
RESUME = 54219

#name_to_data = {}
t = timeit.default_timer()
for i, m in enumerate(meets[RESUME:], start = RESUME):
    status = '[{} / {}] ({})'.format(i, len(meets), m[-1])
    with open('meets/{}_{}.html'.format(m[1], m[0]), 'r') as f:
        ms = bs4.BeautifulSoup(f.read(), 'html.parser')
        for at in ms.find_all('a', href = lambda x: x is not None and x.startswith('http://www.tilastopaja.com/db/at.php?Sex=')):
            if '(' not in at.text: continue # get birth years
            upq = urlparse.parse_qs(urlparse.urlparse(at['href']).query)
            with open('name_to_tilas.csv', 'a', 1) as out:
                orow = [ at.text, upq['Sex'][0], upq['ID'][0], i ]
                print('{} {}'.format(status, orow))
                csv.writer(out).writerow(orow)
                out.flush()

'''
print('writing final data')
with open('real_name_to_tilas.csv', 'a') as out:
    for an in name_to_data.keys():
        orow = [ an ] + name_to_data[an]
        csv.writer(out).writerow(orow)
'''

print(timeit.default_timer() - t)
