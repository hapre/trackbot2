# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import re
import bs4
import elo
import glob
import json
import math
import dill
import pytz
import types
import pyowm
import pprint
import timeit
import athlib
import random
import sqlite3
import datetime
import trueskill
import itertools
import collections
from tzwhere import tzwhere

import tbe
import trackbot_constants
import trackbot_private

def ordinal(n):
    return '%d%s' % (n, 'tsnrhtdd'[(math.floor(n / 10) % 10 != 1) * (n % 10 < 4) * n % 10::4])

def parse_distance(dist, unit = 'm'):
    # dist: string, could be 2 words
    # unit: m, km, or mi
    dist = dist.replace(',', '').strip().lower()
    if dist == 'mile':
        dist = '1609m'
    elif dist == 'nautical mile':
        dist = '1852m'
    elif dist in [ 'mar', 'marathon' ]:
        dist = '42195m'
    elif dist in [ '1/2 marathon', 'half marathon' ]:
        dist = '21098m'
    ds = dist.split()
    if len(ds) == 1:
        ds = [ ''.join(c) for _, c in itertools.groupby(ds[0], key = lambda x : x.isdigit() or x == '.') ]
    miles = [ 'mi', 'miles', 'mile' ]
    meters = [ 'meters', 'meter', 'metres', 'metre' ]
    kilometers = [ 'k', 'km', 'kilometers', 'kilometer', 'kilometres', 'kilometre' ]
    yards = [ 'y', 'yd', 'yds', 'yard', 'yards' ]
    if unit == 'm':
        if ds[-1] in miles:
            ds[0] = str(int(float(ds[0]) * 1609.344))
            ds[-1] = 'm'
        elif ds[-1] in meters:
            ds[0] = str(int(float(ds[0])))
            ds[-1] = 'm'
        elif ds[-1] in kilometers:
            ds[0] = str(int(float(ds[0]) / 1000.0))
            ds[-1] = 'm'
        elif ds[-1] in yards:
            ds[0] = str(int(float(ds[0]) * 0.9144))
            ds[-1] = 'm'
    elif unit == 'km':
        if ds[-1] in miles:
            ds[0] = str(int(float(ds[0]) * 1.609344))
            ds[-1] = 'km'
        elif ds[-1] in meters:
            ds[0] = str(int(float(ds[0]) * 1000.0))
            ds[-1] = 'km'
        elif ds[-1] in kilometers:
            ds[0] = str(int(float(ds[0])))
            ds[-1] = 'km'
    elif unit == 'mi':
        if ds[-1] in miles:
            ds[0] = str(int(float(ds[0])))
            ds[-1] = 'mi'
        elif ds[-1] in meters:
            ds[0] = str(int(float(ds[0]) / 1609.344))
            ds[-1] = 'mi'
        elif ds[-1] in kilometers:
            ds[0] = str(int(float(ds[0]) / 1.609344))
            ds[-1] = 'mi'
    return ds

def parse_time(t, d = '0', seconds = False): # d = distance in meters for hh:mm vs mm:ss
    d = float(d)
    t = t.replace('s', '')
    hours = mins = secs = 0
    if t.count(':') == 2:
        hours, mins, secs = t.split(':')
    elif t.count(':') == 1:
        if d < 10000:
            mins, secs = t.split(':')
        else:
            hours, mins = t.split(':')
    elif t.count(':') == 0:
        secs = t
    if seconds:
        try:
            return 3600 * int(hours) + 60 * int(mins) + float(secs)
        except ValueError:
            return t
    return (int(hours), int(mins), float(secs))

def parse_seconds(t):
    t = float(t)
    h = math.floor(t / (60*60))
    m = math.floor((t - h*60*60) / 60)
    s = round(t - h*60*60 - m*60, 2)
    s = ('0' + str(s)) if s < 10 else str(s)
    if re.search(r'\.[0-9]$', s): s += '0'
    return ':'.join([ str(unit).zfill(2) for unit in [ h, m ] if unit != 0 ] + [ s ])

filt2iaaf = {
    'Mile': '1mile',
    '2 Mile': '2miles',
    'Half Marathon': 'half_marathon',
    'Marathon': 'marathon',
    'High Jump': 'high_jump',
    '110mH': '110mh',
    '400mH': '400mh',
    '2000m Steeplechase': '2000mSt',
    '3000m Steeplechase': '3000mSt',
    'Pole Vault': 'pole_vault',
    'Long Jump': 'long_jump',
    'Triple Jump': 'triple_jump',
    'Shot Put': 'shot_put',
    'Discus': 'discus_throw',
    'Hammer': 'hammer_throw',
    'Javelin': 'javelin_throw',
    'Decathlon': 'decathlon',
    'Heptathlon': 'heptathlon',
}
iaaf2filt = { v: k for k, v in filt2iaaf.items() }
fieldevs = [
    'High Jump', 'Pole Vault', 'Long Jump', 'Triple Jump', 'Shot Put', 'Discus', 'Hammer', 'Javelin', 'Decathlon', 'Heptathlon'
]

def iaafp(gender, evnt, firstresult, indoor = False):
    if firstresult is None: return 0
    if evnt in filt2iaaf: evnt = filt2iaaf[evnt]
    gender = 'f' if 'women' in gender.lower() else 'm'
    evconsts = trackbot_constants.iaaf_constants['2017']['indoor' if indoor else 'outdoor'][gender][evnt]
    points = math.floor(evconsts['conversionFactor'] * ((float(firstresult) + evconsts['resultShift']) ** 2) + evconsts['pointShift'])
    return points

def iaafevents(gender, indoor = False):
    gender = 'f' if 'women' in gender.lower() else 'm'
    return [ iaaf2filt[ev] if ev in iaaf2filt else ev for ev in trackbot_constants.iaaf_constants['2017']['indoor' if indoor else 'outdoor'][gender].keys() ]

def iaafpoints(s, cmd):
    # TrackBot! IAAFPoints 17:13 Men's 5000m
    # sanity check: 4:00 men's outdoor mile = ~1074 pts = ~10.39 men's outdoor 100m
    resp = ''
    args = cmd.split()
    firstresult = parse_time(args[2].replace('m', ''), seconds = True)
    gender = 'f' if 'women' in args[3].lower() else 'm'
    evnt = ' '.join(args[4:])
    if evnt in filt2iaaf:
        ievnt = filt2iaaf[evnt]
    else:
        ievnt = evnt
    evconsts = trackbot_constants.iaaf_constants['2017']['outdoor'][gender][ievnt]
    points = math.floor(evconsts['conversionFactor'] * ((firstresult + evconsts['resultShift']) ** 2) + evconsts['pointShift'])
    resp += 'Equivalent times for {} {} {} (<b>{} points</b>) as per 2017 IAAF tables:<br>\n'.format(args[2], args[3], evnt, points)
    tbcprefix = trackbot_constants.iaaf_constants['2017']['outdoor']
    for otherev in collections.OrderedDict.fromkeys(list(tbcprefix['m'].keys()) + list(tbcprefix['f'].keys())).keys():
        dispev = iaaf2filt[otherev] if otherev in iaaf2filt else otherev
        if otherev in tbcprefix['m']:
            mresult = (-1 if dispev not in fieldevs else 1) * math.sqrt((points - tbcprefix['m'][otherev]['pointShift']) / tbcprefix['m'][otherev]['conversionFactor']) - tbcprefix['m'][otherev]['resultShift']
        else:
            mresult = None
        if otherev in tbcprefix['f']:
            fresult = (-1 if dispev not in fieldevs else 1) * math.sqrt((points - tbcprefix['f'][otherev]['pointShift']) / tbcprefix['f'][otherev]['conversionFactor']) - tbcprefix['f'][otherev]['resultShift']
        else:
            fresult = None
        if dispev not in fieldevs:
            if mresult: mresult = parse_seconds(mresult)
            if fresult: fresult = parse_seconds(fresult)
        else:
            if mresult: mresult = round(mresult, 2)
            if fresult: fresult = round(fresult, 2)
        resp += '{}: {}<br>\n'.format(dispev, ', '.join([ gen for gen in [ '{} (Men)'.format(mresult) if mresult is not None else '', '{} (Women)'.format(fresult) if fresult is not None else '' ] if gen != '' ]))
    return resp

def mcmillan(s, cmd):
    resp = ''
    args = cmd.split()
    dists = [ '100m', '200m', '400m', '500m', '600m', '800m', '1000m', '1500m', '1600m', 'Mile', '2000m', '1.5 Miles', '3000m', '3200m', '2 Miles', '4000m', '3 Miles', '5000m', '6000m', '4 Miles', '8000m', '5 Miles', '10km', '12km', '15km', '10 Miles', '20km', '1/2 Marathon', '15 Miles', '25km', '30km', '20 Miles', 'Marathon', '50km', '50 Miles', '100km', '100 Miles' ]
    d = ' '.join(args[3:])
    for dc in [ d, parse_distance(d, 'm')[0] + 'm', parse_distance(d, 'km')[0] + 'km', parse_distance(d, 'm')[0] + ' Miles' ]:
        if dc in dists:
            rd = dc
            break
    hrs, mins, secs = parse_time(args[2], parse_distance(d, 'm')[0])
    r = s.get('https://www.mcmillanrunning.com/_calculator/calculator.php', params = {
        'hours': hrs,
        'minutes': mins,
        'seconds': secs,
        'distance': rd,
    })
    if r.text == '{"calc": {"original": {"event": ],"workout": ],"velocity": ]}}}':
        return '<i>Wrong params for McMillan. Here are the valid distances: ' + ', '.join(dists) + '</i>'
    resp += 'Equivalent McMillan times for a {} {}:<br>\n<br>\n'.format(args[2], rd)
    for ev in r.json()['calc']['original']['event']:
        resp += '<b>{}:</b> <i>{}</i><br>\n'.format(ev['name'], ev['time'])
    return resp

def athlinks(s, cmd):
    args = cmd.split()
    aname = ' '.join(args[2:])
    censoraname = ' '.join(aname.split()[:-1]) + ' ' + aname.split()[-1][0] + '.'
    resp = ''
    athsearch_d = s.get('https://www.athlinks.com/athlinks/athletes/api/find', params = {
        'term': aname,
        'limit': 3,
    }).json()
    if athsearch_d['result']['total'] == 0:
        resp += '<i>No Athlinks Athlete profiles found for {}</i><br>\n'.format(censoraname)
    else:
        for ath in athsearch_d['result']['athletes']:
            athid = ath['racerId']
            athraces_j = s.get('https://www.athlinks.com/athlinks/athletes/api/{}/Races'.format(athid), params = {
                'spectatorId': athid,
            }).json()
            athprof_j = s.get('https://www.athlinks.com/athlinks/athletes/api/{}'.format(athid)).json()
            censorname = ' '.join(ath['displayName'].split()[:-1]) + ' ' + ath['displayName'].split()[-1][0] + '.'
            censormail = athprof_j['Result']['Email'].replace('@', '(at)')
            resp += 'Recent races for Athlinks athlete profile <b>{}</b> ({}, {}):<br>\n'.format(censorname, athprof_j['Result']['Gender'], censormail)
            for race in athraces_j['Result']['raceEntries']['List'][:5]:
                mycourse = race['Race']['Courses'][0]
                for course in race['Race']['Courses']:
                    if course['CourseID'] == race['CourseID']:
                        mycourse = course
                        break
                resp += '{} for {} Place at {} {} in {}, {} on {}<br>\n'.format(race['TicksString'], ordinal(race['RankG']), race['Race']['RaceName'], mycourse['CourseName'], race['Race']['City'], race['Race']['CountryID3'], race['Race']['RaceDate'].split('T')[0])
    uncsearch_d = s.get('https://www.athlinks.com/athlinks/events/race/result/api/find', params = {
        'term': aname,
        'limit': '5',
    }).json()
    if uncsearch_d['result']['total'] == 0:
        resp += '<i>No unclaimed Athlinks results found for {}</i><br>\n'.format(censoraname)
    else:
        resp += 'Recent unclaimed Athlinks results for <b>{}</b>:<br>\n'.format(censoraname)
        for unc in uncsearch_d['result']['unclaimedResults']:
            resp += '{} at {} {} in {}, {} on {} ({} {})<br>\n'.format(unc['athleteTime'], unc['raceTitle'], unc['courseName'], unc['raceCity'], unc['raceStateProvAbbrev'], unc['raceStartDate'].split('T')[0], unc['athleteGender'], unc['athleteAge'])
    return resp

def qotd(s, cmd):
    resp = ''
    db = sqlite3.connect('quotes.sqlite')
    c = db.cursor()
    c.execute('select * from quotes order by random() limit 1')
    d, q = c.fetchone()
    c.close()
    resp += '<b>Random Letsrun QOTD, from {}:</b><br>\n'.format(d)
    q_s = bs4.BeautifulSoup(q, 'html.parser')
    for script in q_s.find_all('script'):
        script.decompose()
    q_t = q_s.text.replace('\n', '<br>\n')
    resp += q_t
    return resp

def rioath(s, cmd):
    resp = ''
    args = cmd.split()
    fname = ''.join([ c for c in args[2].lower() if c.isalnum() ])
    lname = ''.join([ c for c in args[3].lower() if c.isalnum() ])
    possmatch = None
    athmatch = None
    for athj in glob.glob('athlib/sampledata/rio2016/athletes/*.json'):
        afn = athj[:-5].split('_')[-2]
        aln = athj[:-5].split('_')[-1]
        if afn == fname and aln == lname:
            athmatch = athj
        elif afn == fname or aln == lname:
            possmatch = athj
    if athmatch is None and possmatch is None:
        resp += '<i>No athletes from the Rio Olympics found with name {}</i><br>\n'.format(' '.join(args[2:]))
    if athmatch is None and possmatch is not None:
        athmatch = possmatch
    ath_d = json.load(open(athj))
    resp += 'Info as of 11/2016 for <b>{} {}</b>:<br>\n'.format(ath_d['firstname'], ath_d['lastname'])
    if 'birthplace' in ath_d and ath_d['birthplace'] is not None:
        resp += 'Birthplace: {}<br>\n'.format(ath_d['birthplace'])
    if 'birthdate' in ath_d and ath_d['birthdate'] is not None:
        resp += 'Birthdate (DD.MM.YYYY): {}<br>\n'.format(ath_d['birthplace'])
    if 'weight' in ath_d and ath_d['weight'] is not None:
        resp += 'Weight: {} kilograms<br>\n'.format(ath_d['weight'])
    if 'height' in ath_d and ath_d['height'] is not None:
        resp += 'Height: {} centimeters<br>\n'.format(ath_d['height'])
    if 'nationality' in ath_d and ath_d['nationality'] is not None:
        resp += 'Nationality: {}<br>\n'.format(ath_d['nationality'])
    resp += 'Pre-Rio racing history for <b>{} {}</b>:<br>\n'.format(ath_d['firstname'], ath_d['lastname'])
    resp += ''

def tryfloat(tf):
    try:
        return float(tf)
    except ValueError:
        return tf

def checkfunc(tok, allowed_cmds, tokens):
    for acmd in allowed_cmds:
        if tok.startswith(acmd + '(') and tok.endswith(')'):
            if tok[len(acmd)+1:-1] in tokens:
                return acmd
    return False

def transformdate(ogdate, ogfmt, newfmt):
    try:
        return datetime.datetime.strftime(datetime.datetime.strptime(ogdate, ogfmt), newfmt)
    except ValueError:
        raise tbe.TrackBotError('couldn\'t parse date!')

def tbfilter(s, cmd):
    RESULTLIMIT = 20
    t = timeit.default_timer()
    resp = ''
    args = ' '.join(cmd.split()[2:]).split(',')
    fargs = ' '.join(cmd.split()[2:])
    con = sqlite3.connect('file:tbfilter.sqlite?mode=ro', uri = True)
    cur = con.cursor()
    fields = [ r[1] for r in cur.execute("pragma table_info('tbfilter')").fetchall() ]
    fields.sort(key = len, reverse = True) # important to prevent greedy re scanning
    timeregex = r'(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)(\.(\d{1,9}))?'
    dateregex = r'(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}'
    scanner = re.Scanner([
        (r'|'.join(fields), lambda scanner, token: ('FIELD', token)),
        (r'[0-9]+m', lambda scanner, token: ('IDENTIFIER', token)), # hack for 100m, etc.
        (dateregex + '-' + dateregex, lambda scanner, token: ('DATERANGE', token)),
        (dateregex, lambda scanner, token: ('DATE', token)),
        (timeregex + '-' + timeregex, lambda scanner, token: ('TIMERANGE', token)),
        (timeregex, lambda scanner, token: ('TIME', token)),
        (r',', lambda scanner, token: ('COMMA', token)),
        (r'[a-zA-Z_]+\(', lambda scanner, token: ('FUNC', token)),
        (r'\(', lambda scanner, token: ('LPAREN', token)),
        (r'\)', lambda scanner, token: ('RPAREN', token)),
        (r'=| = | gt | lt ', lambda scanner, token: ('EQUAL', token)),
        (r'[a-zA-Z_]*\*[a-zA-Z_\*]*', lambda scanner, token: ('WILDCARD', token)),
        (r'[a-zA-Z_][a-zA-Z_ ]*', lambda scanner, token: ('IDENTIFIER', token)),
        (r'[\s!]', None), # None == skip token.
    ])
    resp += '<b>Top {} Results for TrackBot filter "{}":</b><br>\n'.format(RESULTLIMIT, fargs)
    toks = scanner.scan(fargs)[0] + [('END', 'END')]
    i = 0
    def incdex():
        nonlocal i, toks
        print(i, toks[i])
        i += 1
        if i >= len(toks):
            raise tbe.TrackBotError('premature end of query<br>\n')
    groups = []
    if toks[i][0] == 'FUNC' and toks[i][1].lower() == 'group(':
        incdex()
        while toks[i][0] != 'RPAREN':
            if i > len(toks):
                resp += 'error: missing ")" in group<br>\n'
                return resp
            if toks[i][0] == 'FIELD':
                groups.append(toks[i][1])
            elif toks[i][0] == 'COMMA':
                pass
            else:
                raise tbe.TrackBotError('invalid token "{}"<br>\n'.format(toks[i][1]))
            incdex()
        incdex() # RPAREN
    print(groups)

    funcs = [ 'count(', 'if(' ]

    # TODO make group 'by'?
    lhses = []
    rhses = []
    while i < len(toks) - 1:
        # begin LHS
        eqlsign = '='
        lhs = [ '', '=?' ]
        if toks[i][0] == 'FIELD':
            lhs[0] = toks[i][1]
            incdex()
        elif toks[i][0] == 'FUNC':
            if toks[i][1] in funcs:
                fullfunc = toks[i][1]
                incdex()
                while toks[i][0] != 'RPAREN':
                    if i > len(toks):
                        raise tbe.TrackBotError('missing ")" in func<br>\n')
                    if toks[i][0] == 'FIELD':
                        fullfunc += toks[i][1]
                    elif toks[i][0] == 'COMMA':
                        pass
                    else:
                        raise tbe.TrackBotError('bad token in func "{}"<br>\n'.format(toks[i][1]))
                    incdex()
                fullfunc += ')'
                lhs[0] = fullfunc
            else:
                raise tbe.TrackBotError('invalid func "{}"<br>\n'.format(toks[i][1]))
            incdex()
        else:
            raise tbe.TrackBotError('bad left-hand-side<br>\n')
        if toks[i][0] != 'EQUAL':
            raise tbe.TrackBotError('invalid left-side token "{}"<br>\n'.format(toks[i][1]))
        eqlsign = '>' if toks[i][1].strip() == 'gt' else '<' if toks[i][1].strip() == 'lt' else '='
        lhs[1] = lhs[1].replace('=', eqlsign)
        incdex()
        # begin RHS + LHS manipulation
        rhss = []
        if toks[i][0] == 'TIME':
            rhss.append(parse_time(toks[i][1], seconds = True))
        elif toks[i][0] == 'TIMERANGE':
            srange, erange = toks[i][1].split('-')
            rhss.append(parse_time(srange, seconds = True))
            rhss.append(parse_time(erange, seconds = True))
            lhs[1] = ' between ? and ?'
        elif toks[i][0] == 'DATE':
            lhs[0] = 'julianday({})'.format(lhs[0])
            lhs[1] = eqlsign + 'julianday(?)'
            rhss.append(transformdate(toks[i][1], '%m/%d/%Y', '%Y-%m-%d'))
        elif toks[i][0] == 'DATERANGE':
            lhs[0] = 'julianday({})'.format(lhs[0])
            lhs[1] = ' between julianday(?) and julianday(?)'
            dates = [ d.strip() for d in toks[i][1].split('-') ]
            rhss.append(transformdate(dates[0], '%m/%d/%Y', '%Y-%m-%d'))
            rhss.append(transformdate(dates[1], '%m/%d/%Y', '%Y-%m-%d'))
        elif toks[i][0] == 'FUNC':
            if toks[i][1] in funcs:
                savedfunc = toks[i][1]
                fullfunc = eqlsign + toks[i][1]
                funcargs = []
                while toks[i][0] != 'RPAREN':
                    if i > len(toks):
                        raise tbe.TrackBotError('missing ")" in func<br>\n')
                    if toks[i][0] == 'FIELD':
                        funcargs.append(toks[i])
                        funcfield = toks[i][1]
                    elif toks[i][0] == 'TIME':
                        funcargs.append((toks[i][0], parse_time(toks[i][1], seconds = True)))
                    incdex()
                if savedfunc == 'count(':
                    fullfunc += '?)'
                    lhs[1] = fullfunc
                    rhss.append(funcfield)
                elif savedfunc == 'if(':
                    if len(funcargs) != 3:
                        raise tbe.TrackBotError('not enough args for "if": should be if(field,then,else)')
                    if funcargs[0][0] != 'FIELD':
                        raise tbe.TrackBotError('if condition must be a field')
                    lhs[1] = eqlsign + ' case when {} then ? else ? end'.format(funcargs[0][1])
                    rhss.append(funcargs[1][1])
                    rhss.append(funcargs[2][1])
            else:
                raise tbe.TrackBotError('error: invalid func "{}"<br>\n'.format(toks[i][1]))
        elif toks[i][0] == 'WILDCARD':
            lhs[1] = ' like ?'
            rhss.append(toks[i][1].replace('*', '%'))
        elif toks[i][0] == 'IDENTIFIER':
            rhss.append(toks[i][1])
        incdex()
        if toks[i][0] == 'COMMA':
            incdex()
        lhses.append(lhs)
        for rhs in rhss:
            rhses.append(rhs)
        print(lhses)
        print(rhses)

    #lhses = [ ('count(dq)', '=count(?)'), ('count(dq)', ' between case when ? then ? else ? end and ?') ]
    #rhses = [ 'round', 'relay', 5, 1, 9999 ]
    cur.execute("select * from tbfilter {} {} order by result asc limit {}".format(
        'where' if len(groups) == 0 else 'group by {} having'.format(','.join(groups)),
        ' and '.join([ '{}{}'.format(left[0], left[1]) for left in lhses ]),
        RESULTLIMIT
    ), rhses)
    #cur.execute('select * from tbfilter where event=? limit 5', [('One Mile', )])
    fetchresults = cur.fetchall()
    cur.close()
    for i, r in enumerate(fetchresults[:RESULTLIMIT], start = 1):
        resp += '{pos}. {msex}{event}{rnd}{heat}{mn} in {city}, {country} {mstart}{mend}: <b>{res}{place}</b> by {first} {last} ({nat}{born}){wind}<br>\n'.format(
            pos = i,
            mn = ' at ' + r[2].split(',')[0] if r[2] not in [ None, '' ] else '',
            city = r[3],
            country = r[4],
            mstart = ('from ' if r[6] != r[5] else 'on ') + r[5].split()[0],
            mend = ' to ' + r[6].split()[0] if r[6] != r[5] else '',
            first = r[8],
            last = r[9],
            nat = r[10],
            msex = r[15] + '\'s ' if r[15] is not None and r[15] != '?' else '',
            event = r[14],
            res = r[22] if r[22] is not None else r[21] if r[21] is not None else r[20],
            place = (' for ' + ordinal(r[24])) if r[24] is not None else '',
            born = (', born ' + (r[11].split()[0] if r[11][4:] != '-01-01' else r[11][:3])) if r[11] is not None else '',
            wind = ' (' + str(r[23]) + ' wind)' if r[23] is not None else '',
            rnd = (' ' + r[17] + ' ') if r[17] not in [ None, '' ] else '',
            heat = r[18] if r[18] not in [ None, '' ] else '',
        )
    if len(fetchresults) == 0:
        resp += '<i>No results found matching that filter</i><br>\n'
    resp += '<i>This filter took {} seconds of computation time</i><br>\n'.format(timeit.default_timer() - t)
    return resp

def db2o(dbr):
    speclist = ( 'year', 'series', 'meet', 'city', 'country', 'meet_start', 'meet_end', 'meet_type', 'fname', 'lname', 'nationality', 'birthdate', 'age', 'gender', 'event', 'evgender', 'relay', 'round', 'heat', 'result', 'resultstring', 'dnf', 'dq', 'wind', 'place', 'hand_timed', 'qualification', 't_cid', 't_aid', 't_eid', 'state', 'score', 'team', 'teamslug', 'grade', 'meet_uuid', 'result_uuid' ) # v1.5
    #speclist = ( 'year', 'series', 'meet', 'city', 'country', 'meet_start', 'meet_end', 'meet_type', 'fname', 'lname', 'nationality', 'birthdate', 'age', 'gender', 'event', 'evgender', 'relay', 'round', 'heat', 'result', 'resultstring', 'dnf', 'dq', 'wind', 'place', 'hand_timed', 'qualification', 't_cid', 't_aid', 't_eid', 'meet_uuid', 'result_uuid' ) # v1.2
    retd = { speclist[i]: dbr[i] for i in range(len(dbr)) }
    return types.SimpleNamespace(**retd)

def average(nums):
    return sum(nums) / len(nums)

def preview(s, cmd):
    # take into account: age, frequency of event, recent perfs as compared to previous perfs
    # TrackBot! Preview Boston Marathon, 04/15/2018, Men's Marathon, Tamirat Tola, Lemi Berhanu, Lelisa Desisa, Nobert Kigen, Wilson Chebet, Evans Chebet, Felix Kandie, Geoffrey Kirui, Philemon Rono, Yuki Kawauchi, Abdi Nageeye, Lusapho April, Arne Gabius, Abdi Abdirahman, Galen Rupp, Reid Coolsaet, Ryan Vail, Stephen Sambu, Eric Gillis, Elkanah Kibet, Timothy Ritchie, Shadrack Biwott, Scott Smith, Andrew Bumbalough
    # TrackBot! Preview London Marathon, 04/21/2018, Women's Marathon, Mary Keitany, Tirunesh Dibaba, Gladys Cherono, Mare Dibaba, Brigid Kosgei, Tigist Tufa, Tadelech Bekele , Rose Chelimo, Vivian Cheruiyot, Charlotte Purdue, Stephanie Bruce, Becky Wade, Tracy Barlow, Lily Partridge, Liz Costello, Rebecca Murray
     # TrackBot! Preview London Marathon, 04/21/2018, Women's Marathon, Mary Keitany (1982), Tirunesh Dibaba (1985), Gladys Cherono (1983), Mare Dibaba (1989), Brigid Kosgei (1994), Tigist Tufa (1987), Tadelech Bekele (1991), Rose Chelimo (1989), Vivian Cheruiyot (1983), Charlotte Purdue (1991), Stephanie Bruce (1984), Becky Wade (1989), Tracy Barlow (1985), Lily Partridge (1991), Liz Costello (1988), Rebecca Murray (1994)
    # TrackBot! Preview London Marathon, 04/21/2018, Men's Marathon, Daniel Wanjiru (1992), Kenenisa Bekele (1982), Eliud Kipchoge (1984), Guye Adola (1990), Stanley Biwott (1986), Abel Kirui (1982), Lawrence Cherono (1988), Tola Shura (1996), Bedan Muchiri (1990), Ghirmay Ghebreslassie (1995), Yohanes Gebregergish (1994), Amanuel Mesel (1990), Mo Farah (1983), Alphonce Felix (1992), Tatsunori Hamasaki (1988), Fernando Cabada (1982), Ihor Olefirenko (1990), Tsegai Tewelde (1989), Jonathan Mellor (1986), Samuel Chelanga (1985), Aaron Scott (1990), Stephen Scullion (1988), Taher Belkorchi (1990), Matthew Clowes (1989)
    resp = ''
    args = ' '.join(cmd.split()[2:]).split(', ')
    atnames = [ ( re.sub(r'[ ]*\([0-9]{4}\)', '', a), re.search(r'[0-9]{4}', a).group(0) if re.search(r'[0-9]{4}', a) is not None else None ) for a in args[3:] ]
    prevmeet = []
    meetname = args[0]
    meetdate = datetime.datetime.strptime(args[1], '%m/%d/%Y')
    try:
        with open('prevcache.pkl', 'rb') as f:
            prevcache = dill.load(f)
    except:
        prevcache = {}
    if meetname in [ 'top50s' ]: raise tbe.TrackBotError('bad meet name')
    #del(prevcache['top50s'])
    if 'top50s' not in prevcache:
        print('fetching top50s')
        prevcache['top50s'] = {}
        con = sqlite3.connect('file:tbfilter.sqlite?mode=ro', uri = True)
        cur = con.cursor()
        for gen in [ 'Men', 'Women' ]:
            prevcache['top50s'][gen] = {}
            for evnt in [ '100m', 'shorth', '200m', '400m', '400mH', '800m', '1500m', 'Mile', '3000m', '3000m Steeplechase', '5000m', '10000m', 'Half Marathon', 'Marathon' ]:
                if evnt == 'shorth': evnt = '100mH' if gen == 'Women' else '110mH'
                print('fetching top50s {} {}'.format(gen, evnt))
                cur.execute('select * from tbfilter where event=? and evgender=? and result is not null and dq is null and not instr(resultstring, \'w\') order by result asc limit 50', (evnt, gen))
                prevcache['top50s'][gen][evnt] = [ db2o(p) for p in cur.fetchall() ]
                print(len(prevcache['top50s'][gen][evnt]))
                print(prevcache['top50s'][gen][evnt][0])
            with open('prevcache.pkl', 'wb') as f:
                dill.dump(prevcache, f)
        cur.close()
    a2rs = {}
    for atn in atnames:
        if '!' in atn[0]: # bypass cache
            atn[0] = atn[0].replace('!', '')
            del(prevcache[atn[0]])
        if atn[0] not in prevcache:
            print('fetching {}'.format(atn))
            con = sqlite3.connect('file:tbfilter.sqlite?mode=ro', uri = True)
            cur = con.cursor()
            splitatn = atn[0].split()
            if len(splitatn) == 2:
                searchfn = splitatn[0]
                searchln = splitatn[1]
            elif len(splitatn) == 3: # e.g. Wayde van Niekerk
                searchfn = splitatn[0]
                searchln = ' '.join(splitatn[1:])
            elif len(splitatn) == 4: # e.g. Marie Josée Ta Lou
                searchfn = ' '.join(splitatn[:2])
                searchln = ' '.join(splitatn[2:])
            cur.execute('select * from tbfilter where fname=? and lname=? limit 1000', (searchfn, searchln))
            prevcache[atn[0]] = [ db2o(p) for p in cur.fetchall() ]
            cur.close()
            if len(prevcache[atn[0]]) > 0:
                print(prevcache[atn[0]][0])
            with open('prevcache.pkl', 'wb') as f:
                dill.dump(prevcache, f)
        a2rs[atn] = {}
        a2rs[atn]['perfs'] = sorted([ p for p in prevcache[atn[0]] if (p.birthdate and p.birthdate[:4] == atn[1]) or atn[1] is None ], key = lambda x: x.meet_start, reverse = True)
        print('{} -> {}'.format(len(prevcache[atn[0]]), len(a2rs[atn]['perfs'])))
    for atn in atnames:
        print(atn, len(a2rs[atn]['perfs']))
    if meetname not in prevcache:
        print('fetching meet "{}"'.format(meetname))
        con = sqlite3.connect('file:tbfilter.sqlite?mode=ro', uri = True)
        cur = con.cursor()
        if meetname.endswith(' DL'):
            realcity = ' '.join(meetname.split()[:-1])
            cur.execute('select * from tbfilter where series=? and city=? limit 10000', ('Diamond', realcity))
        else:
            cur.execute('select * from tbfilter where meet like ? limit 10000', ('%'+meetname+'%',))
        prevcache[meetname] = [ db2o(p) for p in cur.fetchall() ]
        print('meets found: {}'.format(len(prevcache[meetname])))
        cur.close()
        with open('prevcache.pkl', 'wb') as f:
            dill.dump(prevcache, f)
    pevgen = args[2].split()[0].replace('\'s', '')
    pevent = args[2].split()[1]
    lastmeetyear = sorted([ p.year for p in prevcache[meetname] if p.year < meetdate.year and p.event == pevent and p.evgender == pevgen ], reverse = True)[0]
    lastmeet = sorted([ p for p in prevcache[meetname] if p.year == lastmeetyear and p.event == pevent and p.evgender == pevgen and p.round == '' and p.heat == '' and p.result ], key = lambda x: x.place if x.place is not None else 1e10) # TODO fix when round and heat are none if empty
    twomeetsagoyear = sorted([ p.year for p in prevcache[meetname] if p.year < lastmeetyear and p.event == pevent and p.evgender == pevgen ], reverse = True)[0]
    twomeetsago = sorted([ p for p in prevcache[meetname] if p.year == twomeetsagoyear and p.event == pevent and p.evgender == pevgen and p.round == '' and p.heat == '' and p.result ], key = lambda x: x.place if x.place is not None else 1e10) # TODO fix when round and heat are none if empty
    fakelastmeet = lastmeet + ([ lastmeet[-1] ] * (max(len(lastmeet), len(twomeetsago), len(atnames)) - len(lastmeet)))
    predtimes = [ round(average([ fakelastmeet[pl].result, twomeetsago[pl].result ]), 2) for pl in range(len(fakelastmeet)) ]
    for pl, pt in enumerate(predtimes):
        if pl == 0: continue
        if pt < predtimes[pl - 1]:
            predtimes[pl] = predtimes[pl - 1] - pt + 0.01
    predstrtimes = [ parse_seconds(pt) for pt in predtimes ]
    predwind = round(average([ lastmeet[0].wind, twomeetsago[0].wind ]), 1) if lastmeet[0].wind is not None and twomeetsago[0].wind is not None else None
    meetloc = '{},{} {}'.format(lastmeet[0].city, ' {},'.format(lastmeet[0].state) if False else '', lastmeet[0].country)
    weathertup = (meetloc, meetdate)
    if weathertup not in prevcache:
        print('fetching weather for {} at {}'.format(meetloc, meetdate))
        owm = pyowm.OWM(trackbot_private.owmkey)
        forecaster = owm.daily_forecast(meetloc)
        forecastloc = forecaster.get_forecast().get_location()
        latlon = (forecastloc.get_lat(), forecastloc.get_lon())
        timezone = pytz.timezone(tzwhere.tzwhere().tzNameAt(latlon[0], latlon[1]))
        #meetdate = meetdate.replace(tzinfo = timezone) TODO breaks when compared to naive dates
        meetdate = meetdate.replace(hour = 12)
        nowtime = datetime.datetime.now()
        delta = meetdate - nowtime
        if delta.days > 0 and delta.days <= 5:
            prevcache[weathertup] = json.loads(forecaster.get_weather_at(meetdate).to_JSON())
        else:
            #weathertup = (meetloc, forecaster.when_starts('date')) # TODO massive hack
            prevcache[weathertup] = json.loads(forecaster.get_weather_at(forecaster.when_starts('date')).to_JSON())
        with open('prevcache.pkl', 'wb') as f:
            dill.dump(prevcache, f)
    pprint.pprint(prevcache[weathertup])
    tsenv = trueskill.TrueSkill()
    for atn in atnames:
        a2rs[atn]['pbs'] = sorted([ p for p in a2rs[atn]['perfs'] if p.event == pevent ], key = lambda x: x.result if x.result is not None else 1e10)
        a2rs[atn]['pb'] = a2rs[atn]['pbs'][0] if len(a2rs[atn]['pbs']) > 0 else None
        a2rs[atn]['bestperfs'] = sorted([ ( iaafp(p.evgender, p.event, p.result, indoor = True if p.meet_type == 'Indoor' else False), p ) for p in a2rs[atn]['perfs'] if p.event in iaafevents(p.evgender, indoor = True if p.meet_type == 'Indoor' else False) ], reverse = True, key = lambda x: x[0])
        a2rs[atn]['bestperf'] = a2rs[atn]['bestperfs'][0]
        birthdate = datetime.datetime.strptime(a2rs[atn]['bestperf'][1].birthdate[:10], '%Y-%m-%d')
        past2years = [ ip for ip in a2rs[atn]['bestperfs'] if datetime.datetime.strptime(ip[1].meet_start[:10], '%Y-%m-%d') > meetdate.replace(year = meetdate.year - 2) ]
        a2rs[atn]['bestperfs_past2years'] = past2years
        a2rs[atn]['bestperf_past2years'] = past2years[0] if len(past2years) > 0 else (0, 0)
        pastyear = [ ip for ip in a2rs[atn]['bestperfs_past2years'] if datetime.datetime.strptime(ip[1].meet_start[:10], '%Y-%m-%d') > meetdate.replace(year = meetdate.year - 1) ]
        a2rs[atn]['bestperf_pastyear'] = pastyear[0] if len(pastyear) > 0 else (0, 0)
        a2rs[atn]['age'] = meetdate.year - birthdate.year - ((meetdate.month, birthdate.day) < (meetdate.month, birthdate.day))
        a2rs[atn]['medals'] = [ p for p in a2rs[atn]['perfs'] if (p.series == 'WC' or p.series == 'OG') and not(p.round) and not(p.heat) and p.place and p.place <= 3 ]
        a2rs[atn]['raceperfs'] = [ p for p in a2rs[atn]['perfs'] if meetname in p.meet ]
        a2rs[atn]['rating'] = tsenv.create_rating()
        a2rs[atn]['elo'] = elo.Rating(1000)
        places = [ p.place for p in a2rs[atn]['perfs'] if p.place ]
        a2rs[atn]['avgplace'] = round(sum(places) / len(places), 2)
        winstreak = 0
        while a2rs[atn]['perfs'][winstreak].place is not None and a2rs[atn]['perfs'][winstreak].place == 1:
            winstreak += 1
        a2rs[atn]['winstreak'] = winstreak
        winstreak_ev = 0
        winstreak_idx = 0
        while winstreak_idx < len(a2rs[atn]['perfs']):
            while winstreak_idx < len(a2rs[atn]['perfs']) and a2rs[atn]['perfs'][winstreak_idx].event != pevent:
                winstreak_idx += 1
            p = a2rs[atn]['perfs'][winstreak_ev]
            if p.place is not None and p.place == 1:
                winstreak_ev += 1
            else:
                break
        a2rs[atn]['winstreak_ev'] = winstreak_ev
        a2rs[atn]['worstperfs_past2years'] = past2years[::-1][:3]
        a2rs[atn]['iaaf_lastperf'] = [ ( iaafp(p.evgender, p.event, p.result, indoor = True if p.meet_type == 'Indoor' else False), p ) for p in a2rs[atn]['perfs'] if p.event in iaafevents(p.evgender, indoor = True if p.meet_type == 'Indoor' else False) ][0]
        a2rs[atn]['variance'] = round(a2rs[atn]['bestperfs_past2years'][0][0] - average([ a[0] for a in a2rs[atn]['worstperfs_past2years'] ]), 2) if len(a2rs[atn]['worstperfs_past2years']) == 3 else 0 # iaafp(best last 2 years) - iaafp(avg of 3 worst)
        lastwins = [ p for p in a2rs[atn]['perfs'] if p.place == 1 and p.event == pevent ]
        a2rs[atn]['lastwin'] = lastwins[0] if len(lastwins) > 0 else None
        a2rs[atn]['top50s_ev'] = [ p for p in prevcache['top50s'][pevgen][pevent] if p.fname == atn[0].split()[0] and p.lname == atn[0].split()[-1] and (p.birthdate[:4] == atn[1] if atn[1] else True) ]
        a2rs[atn]['top50s_all'] = {}
        for t5r in list(itertools.chain.from_iterable(prevcache['top50s'][pevgen].values())):
            if t5r.fname == atn[0].split()[0] and t5r.lname == atn[0].split()[-1] and (t5r.birthdate[:4] == atn[1] if atn[1] else True):
                if t5r.event not in a2rs[atn]['top50s_all']: a2rs[atn]['top50s_all'][t5r.event] = []
                a2rs[atn]['top50s_all'][t5r.event].append(t5r)
        a2rs[atn]['hometown_hero'] = a2rs[atn]['bestperf'][1].nationality == lastmeet[0].country
        a2rs[atn]['nationality'] = a2rs[atn]['bestperf'][1].nationality
        a2rs[atn]['totalwins'] = sum([ p.place is not None and p.place == 1 for p in a2rs[atn]['perfs'] ])
        a2rs[atn]['totalwins_ev'] = sum([ p.place is not None and p.place == 1 and p.event == pevent for p in a2rs[atn]['perfs'] ])

    matchups = []
    for atn in atnames:
        for atn2 in atnames:
            if atn2 == atn: continue
            for p in a2rs[atn]['perfs']:
                for p2 in a2rs[atn2]['perfs']:
                    if p.meet_uuid == p2.meet_uuid and p.event == p2.event and p.round == p2.round and p.heat == p2.heat and p.result and p2.result and p.result != p2.result:
                        meet_uuid, meet_date = p.meet_uuid, p.meet_start
                        winner = atn if p.result < p2.result else atn2
                        loser = atn2 if p.result < p2.result else atn
                        winres = p.result if p.result < p2.result else p2.result
                        loseres = p2.result if p.result < p2.result else p.result
                        match = ( meet_uuid, meet_date, winner, winres, loser, loseres, p.round, p.heat, p.meet )
                        if match not in matchups:
                            matchups.append(match)
    for m in sorted(matchups, key = lambda x: datetime.datetime.strptime(x[1][:10], '%Y-%m-%d')):
        atn, atn2 = m[2], m[4]
        (newr1,), (newr2,) = tsenv.rate([ (a2rs[atn]['rating'],), (a2rs[atn2]['rating'],) ], ranks = [ m[3], m[5] ])
        a2rs[atn]['rating'] = newr1
        a2rs[atn2]['rating'] = newr2
        elowin, eloloss = elo.rate_1vs1(a2rs[atn]['elo'], a2rs[atn2]['elo'])
        a2rs[atn]['elo'] = elowin
        a2rs[atn2]['elo'] = eloloss

    atbysf = {}
    atbysf['iaafbest'] = [ lambda x: a2rs[x]['bestperf'][0], True ] # sort on atnames, reverse
    atbysf['iaafbest_pastyear'] = [ lambda x: a2rs[x]['bestperf_pastyear'][0], True ]
    atbysf['iaafbest_past2years'] = [ lambda x: a2rs[x]['bestperf_past2years'][0], True ]
    atbysf['pb'] =  [ lambda x: ( a2rs[x]['pb'].result, a2rs[x]['pb'].resultstring ) if a2rs[x]['pb'] else (1e10, ''), False ]
    atbysf['age'] = [ lambda x: a2rs[x]['age'], False ]
    atbysf['experience'] = [ lambda x: len(a2rs[x]['pbs']), True ]
    atbysf['eventexp'] = [ lambda x: len(a2rs[x]['raceperfs']), True ]
    atbysf['averageplace'] = [ lambda x: a2rs[x]['avgplace'], False ]
    atbysf['medalcount'] = [ lambda x: len(a2rs[x]['medals']), True ]
    atbysf['trueskill'] = [ lambda x: a2rs[x]['rating'], True ]
    atbysf['elo'] = [ lambda x: round(float(a2rs[x]['elo']), 2), True ]
    atbysf['depth'] =  [ lambda x: a2rs[x]['pbs'][3].result if len(a2rs[x]['pbs']) >= 4 else 1e10, False ]
    atbysf['winstreak'] =  [ lambda x: a2rs[x]['winstreak'], True ]
    atbysf['variance'] =  [ lambda x: a2rs[x]['variance'], False ]
    atbysf['iaaf_lastperf'] =  [ lambda x: a2rs[x]['iaaf_lastperf'][0], True ]
    atbysf['winstreak_ev'] =  [ lambda x: a2rs[x]['winstreak_ev'], True ]
    atbysf['months_sincelastwin'] = [ lambda x: ((meetdate.year * 12 + meetdate.month) - (a2rs[x]['lastwin'].year * 12 + datetime.datetime.strptime(a2rs[x]['lastwin'].meet_start[:10], '%Y-%m-%d').month)) if a2rs[x]['lastwin'] else 1e10, False ]
    atbysf['top50s_ev'] = [ lambda x: len(a2rs[x]['top50s_ev']), True ]
    atbysf['top50s_all'] = [ lambda x: sum(len(a2rs[x]['top50s_all'][e]) for e in a2rs[x]['top50s_all'].keys()), True ]
    atbysf['hometown_hero'] = [ lambda x: a2rs[x]['hometown_hero'], True ]
    atbysf['totalwins'] = [ lambda x: a2rs[x]['totalwins'], True ]
    atbysf['totalwins_ev'] = [ lambda x: a2rs[x]['totalwins_ev'], True ]
    atby = {}
    for crit in atbysf.keys():
        atby[crit] = sorted(a2rs.keys(), key = atbysf[crit][0], reverse = atbysf[crit][1])
    weights = { # Note: higher = more important
        'pb': 5,
        'iaafbest': 4,
        'iaafbest_pastyear': 4,
        'trueskill': 1,
        'elo': 3,
        'medalcount': 2,
    }
    for atn in atnames:
        a2rs[atn]['weight'] = sum([ (atby[w].index(atn) + 1) * weights[w] for w in weights.keys() ])
    atbysf['weighted'] = [ lambda x: a2rs[x]['weight'], False ]
    atby['weighted'] = sorted(a2rs.keys(), key = atbysf['weighted'][0], reverse = atbysf['weighted'][1])
    def surname(at):
        atspl = at[0].split()
        if len(atspl) == 2:
            return atspl[1]
        elif len(atspl) > 3:
            return ' '.join(atspl[-2:])
    def noref(num, criteria = 'weighted'):
        if num < 0: num += 1
        return atby[criteria][num-1]
    mentioned = []
    def refa(at, s = False):
        nonlocal mentioned
        if at in mentioned:
            ret = surname(at)
            if s: ret += ' also'
        else:
            ret = '<strong>{}</strong>'.format(at[0])
        mentioned.append(at)
        return ret
    def ref(num, criteria = 'weighted', s = False):
        nonlocal mentioned
        if num < 0: num += 1
        at = atby[criteria][num-1]
        if at in mentioned:
            ret = surname(at)
            if s: ret += ' also'
        else:
            ret = '<strong>{}</strong>'.format(at[0])
        mentioned.append(at)
        return ret
    def byget(num, criteria = 'weighted'):
        if num < 0: num += 1
        return atbysf[criteria][0](atby[criteria][num-1])
    def reldate(dt):
        if isinstance(dt, str):
            dt = datetime.datetime.strptime(dt[:10], '%Y-%m-%d')
        if dt > meetdate.replace(year = meetdate.year - 1):
            return dt.strftime('last %B')
        elif dt > meetdate.replace(year = meetdate.year - 2):
            return dt.strftime('in %B two years ago')
        else: 
            return dt.strftime('in %B %Y')
    def exists(cond, num, greater = True, depth = 1):
        count = 0
        for at in atnames:
            if greater and atbysf[cond][0](at) >= num:
                count += 1
            elif (not greater) and atbysf[cond][0](at) <= num:
                count += 1
        return count >= depth
    def ties(num, criteria):
        if num < 0: num += 1
        return [ a for a in atby[criteria] if atbysf[criteria][0](a) == byget(num, criteria = criteria) and a != atby[criteria][num-1] ]
    stored = None
    def store(perf, attr = None):
        nonlocal stored
        stored = perf
        return getattr(perf, attr) if attr else perf
    def gsto():
        return stored
    stored2 = None
    def store2(obj):
        nonlocal stored2
        stored2 = obj
        return obj
    def gsto2():
        return stored2

    men = True if 'M' in pevgen else False
    tbpfn = [ a[0] for a in atby['weighted'] ]
    tbpln = [ a[0].split()[-1] for a in atby['weighted'] ]

    titles = [
        '{} searches for glory in {}',
        '{}{}{}',
    ]
    intros = [
        lambda: 'A slew of competitors led by stars {} and {} will be looking to take home the crown in the {} edition of the {} {} {}.'.format(
            ref(1), ref(2),
            meetdate.year,
            meetname, 'Men\'s' if men else 'Women\'s', pevent
        ),
        lambda: '{} may be the {} of {} running, but will {} be the day {}he goes down?'.format(
            ref(1), 'king' if men else 'queen', pevent,
            meetdate.strftime('%B') + ' ' + ordinal(meetdate.day),
            '' if men else 's'
        ),
    ]
    positive_bodies = [ # list of ( condition, subject, text ) tuples
        [ exists('winstreak_ev', 4), noref(1, 'winstreak_ev'), lambda: '{} has won each of the last {} {}s {}\'s competed in -- that gives {} the highest winstreak coming into the race{}.'.format(
            ref(1, 'winstreak_ev'), byget(1, 'winstreak_ev'), pevent, 
            'he' if men else 'she', 'him' if men else 'her',
            ', matched by {}'.format(' and '.join([ tie[0] for tie in ties(1, 'winstreak_ev') ])) if len(ties(1, 'winstreak_ev')) > 0 else ''
        ) ],
        [ exists('winstreak', 4), noref(1, 'winstreak'), lambda: 'Including all {} races, {} has won each of the last {} races {}\'s competed in -- that gives {} the highest winstreak coming into the race{}.'.format(
            'his' if men else 'her', ref(1, 'winstreak'), byget(1, 'winstreak'),
            'he' if men else 'she', 'him' if men else 'her',
            ', matched by {}'.format(' and '.join([ tie[0] for tie in ties(1, 'winstreak') ])) if len(ties(1, 'winstreak')) > 0 else ''
        ) ],
        [ exists('medalcount', 3), noref(1, 'medalcount'), lambda: 'If there\'s one thing that\'s certain about {}, it\'s that {} knows how to win when it counts. {} has won {} global medals over {} career, the most in the field.'.format(
            ref(1, 'medalcount'), 'he' if men else 'she', ref(1, 'medalcount'), byget(1, 'medalcount'), 'his' if men else 'her'
        ) ],
        [ True, noref(1, 'iaafbest'), lambda: '{} shocked the world with {} amazing {} performance {} -- {} {} clocking was the best of {} career{}.'.format(
            ref(1, 'iaafbest'), 'his' if men else 'her', store(a2rs[noref(1, 'iaafbest')]['bestperf'][1], 'event'), reldate(gsto().meet_start),
            'his' if men else 'her', gsto().resultstring, 'his' if men else 'her', ', and {} beat out fellow {} racers {} there as well'.format(
                'he' if men else 'she', lastmeet[0].city,
                ' and '.join(gsto2())) if len(store2([ refa(m[4]) for m in matchups if m[0] == gsto().meet_uuid ])) > 0 else ''
        ) ],
        [ exists('hometown_hero', True), noref(1, 'hometown_hero'), lambda: '{} will be one of the top hometown hopes for the crowd to cheer for.'.format(
            ref(1, 'hometown_hero')
        ) ],
    ]
    negative_bodies = [
        [ exists('variance', 500), noref(-1, 'variance'), lambda: '{}\'s inconsistency might do {} over. At {} best, {}\'s fantastic, as {} {} {} in {} {} showed. '.format(
            ref(-1, 'variance'), 'him' if men else 'her', 'his' if men else 'her', 'he' if men else 'she', 'his' if men else 'her',
            store(a2rs[noref(-1, 'variance')]['bestperf_past2years'][1], 'resultstring'), gsto().event, gsto().city, reldate(gsto().meet_start)) +
          'But {} {} {} in {} {} and {} {} {} in {} {} seem to paint the picture of a very different athlete.'.format(
              'his' if men else 'her',
              store(a2rs[noref(-1, 'variance')]['worstperfs_past2years'][0][1], 'resultstring'), gsto().event, gsto().city, reldate(gsto().meet_start),
              'his' if men else 'her',
              store(a2rs[noref(-1, 'variance')]['worstperfs_past2years'][1][1], 'resultstring'), gsto().event, gsto().city, reldate(gsto().meet_start),
        ) ],
        [ True, noref(-1, 'iaaf_lastperf'), lambda: '{} is coming off arguably the worst tuneup race of the bunch, with {} lackluster {} {} in {} {} leaving something to be desired going up against the likes of {}\'s {} {} in {} {}.'.format(
            ref(-1, 'iaaf_lastperf'), 'his' if men else 'her',
            store(a2rs[noref(-1, 'iaaf_lastperf')]['iaaf_lastperf'][1], 'resultstring'), gsto().event, gsto().city, reldate(gsto().meet_start),
            ref(1, 'iaaf_lastperf'),
            store(a2rs[noref(1, 'iaaf_lastperf')]['iaaf_lastperf'][1], 'resultstring'), gsto().event, gsto().city, reldate(gsto().meet_start),
        ) ],
        [ atbysf['months_sincelastwin'][0](noref(1)) > 12, noref(1), lambda: 'But there\'s nothing certain about what\'s going down on {}. Despite being one of the favorities, if {} were to win this year\'s race, it would be {} first {} win in {} months -- {} last being in {} {}.'.format(
            meetdate.strftime('%A'), ref(1), 'his' if men else 'her', pevent, atbysf['months_sincelastwin'][0](noref(1)),
            'his' if men else 'her', a2rs[noref(1)]['lastwin'].city, reldate(a2rs[noref(1)]['lastwin'].meet_start)
        ) ],
        [ exists('age', 30), noref(-1, 'age'), lambda: 'At {} years young, {} will be {}the oldest in the field on {} -- and that\'s getting up there for a {} runner.'.format(
            byget(-1, 'age'),
            ref(-1, 'age'),
            'tied for ' if len(ties(-1, 'age')) > 0 else '',
            meetdate.strftime('%A'),
            pevent
            
        ) ],
    ]
    #for pos in positive_bodies:
    #    resp += '{}<br>\n'.format(pos)
    #for neg in negative_bodies:
    #    resp += '{}<br>\n'.format(neg)

    badtrans = [
        'But there\'s some worries about {} as well. ',
        'But alas, not all is good with {}. ',
        'However {{}} still stands to be challenged in {}. '.format(lastmeet[0].city),
        'But there are some important points going against {}. ',
        'But that\'s not to say {} isn\'t immune to criticism. ',
    ]

    def athintro(atno):
        if atno == 1:
            firsts = [
                'First, let\'s talk about {}. ',
                'The discussion starts with {}. ',
            ]
            return random.choice(firsts).format(ref(atno))
        elif atno == len(atnames): # remember, one-indexed
            lasts = [
                'And last but not least, there\'s {}. ',
                'Not to be forgotten, {} rounds out the field. '
            ]
            return random.choice(lasts).format(ref(atno))
        else:
            middles = [
                '{} will also be one to watch. ',
                'Among {} competitors will be {{}}. '.format('his' if men else 'her'),
                'Also expecting to stick {} nose in the fight will be {{}}. '.format('his' if men else 'her'),
            ]
            return random.choice(middles).format(ref(atno))

    resp += '{} <br><br>\n\n'.format(random.choice(intros)())
    for atno, at in enumerate(atby['weighted'], start = 1):
        resp += athintro(atno)
        for opi in positive_bodies:
            if opi[0] and opi[1] == at:
                resp += '{} '.format(opi[2]())
        resp += random.choice(badtrans).format(surname(at))
        for opi in negative_bodies:
            if opi[0] and opi[1] == at:
                resp += '{} '.format(opi[2]())
        resp += '<br><br>\n\n'

    resp += 'Here are my predictions for this race{}:<br>\n'.format(' (assuming {}m/s wind)'.format(predwind) if predwind else '')
    for atno, at in enumerate(atby['weighted'], start = 1):
        resp += '{}. {}, {}<br>\n'.format(atno, at[0], predstrtimes[atno])
    
    def new_rating_repr(self):
        c = type(self)
        args = (self.mu, self.sigma)
        return 'mu=%.2f,sigma=%.2f' % args
    setattr(trueskill.Rating, '__repr__', new_rating_repr)
    resp += 'Here\'s the data I calculated to write this preview:<br>\n'
    for crit in atby.keys():
        resp += 'Athletes by {} ({} is good): {}<br>\n'.format(crit, 'high' if atbysf[crit][1] else 'low', ', '.join([ '{}. {}. {} ({})'.format(c + 1, atby[crit][c][0].split()[0][0], atby[crit][c][0].split()[-1], atbysf[crit][0](atby[crit][c])) for c in range(len(atby[crit])) ]))

    return resp

def workout(s, cmd):
    # TrackBot! Workout for 5000m: 3:50 1200m, 3:00 rest, 3:40 1200m, 2:00 rest
    # concept: average iaafpoints. add iaafpoints for short rest.
    # concept: sum times and distances. remove iaafpoints based on rest
    args = args.replace(' for', '')
    target = args.split()[2].replace(':', '')
    args = [ rep.strip() for rep in ' '.join(cmd.split()[3:]).split(',') ]
    args = [ parse_time(, seconds = True) ]
    
    
    return 'if done at a moderate effort, this workout indicates:'

funcs = {
    'mcmillan': mcmillan,
    'athlinks': athlinks,
    'iaafpoints': iaafpoints,
    'qotd': qotd,
    'rioath': rioath,
    'filter': tbfilter,
    'preview': preview,
    'workout': workout,
}
