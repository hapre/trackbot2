#!/usr/pkg/bin/python3

##!/bin/sh
#tail -n +3 "$0" | python3; exit # hack for Guix compatibility (no /usr)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import re
import bs4
import sys
import json
import html
import pprint
import sqlite3
import requests

import tbe
import tbf
import trackbot_private

s = requests.Session()
defheaders = { 'User-Agent': 'TrackBot/2.0 <http://habs.sdf.org/>' }
s.headers.update(defheaders)

testmode = True
if __name__ == '__main__':
    testmode = True

def get_latest_post():
    fp_s = bs4.BeautifulSoup(s.get('http://www.letsrun.com/forum/index.php').text, 'html.parser')
    row_s = fp_s.find('ul', class_ = 'thread_list').find_all('li', class_ = 'row')[2]
    thread_u = row_s.find('span', class_ = 'post_title').find('a')['href']
    thread_s = bs4.BeautifulSoup(s.get(thread_u).text, 'html.parser')
    pag_s = thread_s.find('ul', class_ = 'pagination')
    if pag_s:
        thread_s = bs4.BeautifulSoup(s.get(pag_s.find_all('li')[-2].find('a')['href']).text, 'html.parser')
    return thread_s.find_all('a', { 'name': True })[-1]['name']

latest_post = '8693958' if testmode else get_latest_post()

def parse(cmd, credit = None):
    if credit:
        s.headers.update({ 'User-Agent': 'TrackBot/2.0 <http://habs.sdf.org/>, requested by LetsRun user "{}"'.format(credit) })
    tbffunc = cmd.split()[1].lower()
    try:
        result = tbf.funcs[tbffunc](s, cmd)
    except tbe.TrackBotError as e:
        if len(e.args) > 0:
            result = 'Error in command "{}": {}'.format(tbffunc, e.args[0])
        else:
            result = 'DEVASTATING error in command "{}"'.format(tbffunc)
    s.headers.update(defheaders)
    return result

resume_post = int(open('post', 'r').read().strip()) + 1
for post in range(int(resume_post), int(latest_post) + 1):
    if not testmode:
        open('post', 'w').write(str(post))
    if testmode:
        post_s = bs4.BeautifulSoup(open('8693958.html').read(), 'html.parser')
    else:
        post_r = s.get('https://www.letsrun.com/forum/posts/{}/reply'.format(post))
        if post_r.status_code == 404:
            continue
        post_s = bs4.BeautifulSoup(post_r.text, 'html.parser')
    for jsline in post_s.find('script').text.split('\n'):
        if jsline.strip().startswith('window.App.state.reply ='):
            post_d = json.loads(''.join(jsline.split('=')[1:])[:-1])
            break
    db = sqlite3.connect('trackbot.sqlite')
    c = db.cursor()
    # n.b. format() is only used for ?s and keys -- sqlite parameter substitution is used for all values, so sql injection via values isn't possible
    c.execute('create table if not exists posts ({})'.format(','.join(post_d.keys())))
    if not testmode:
        c.execute('insert into posts ({}) values ({})'.format(','.join(post_d.keys()), ','.join('?' * len(post_d.values()))), tuple( str(v) for v in post_d.values() ))
    db.commit()
    c.close()
    resp = ''
    body_s = bs4.BeautifulSoup(post_d['body_html'], 'html.parser')
    for bq in body_s.find_all('blockquote'):
        bq.decompose()
    for line in html.escape(str(body_s)).split('\n'):
        if line.lower().startswith('trackbot!'):
            resp += parse(line, credit = post_d['author'])
    if testmode:
        resp += parse(' '.join(sys.argv[1:]))
    if resp.strip() != '':
        tbpd = {
            'secret': trackbot_private.secret,
            'reply_to': int(post_d['id']),
            'subject': '{}{}'.format('RE: ' if 'RE: ' not in post_d['subject'] else '', post_d['subject']),
            'body': resp,
        }
        if testmode:
            pprint.pprint(tbpd)
            print(resp)
        else:
            s.post('https://www.letsrun.com/api/forum/trackbot', json = tbpd)
    if testmode: exit()
