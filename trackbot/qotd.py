#!/usr/bin/env python3

import os
import re
import bs4
import sqlite3
import requests
import datetime
import dateutil
import dateutil.rrule

# first: nov 21, 2006
#start = datetime.datetime(2006, 11, 21)
start = datetime.datetime(2009, 1, 5)

db = sqlite3.connect('quotes.sqlite')
c = db.cursor()
c.execute('create table if not exists quotes(date primary key, quote)')

def qotd_untitle(s):
    return s.text.replace('of', '').replace('the', '').replace('for', '')

for dt in dateutil.rrule.rrule(dateutil.rrule.DAILY, dtstart = start, until = datetime.datetime.now()):
    dt_slash = datetime.datetime.strftime(dt, '%Y/%m/%d')
    fp_t = requests.get('https://www.letsrun.com/archive/{}/'.format(dt_slash)).text
    fp_fname = 'fp/{}.html'.format(dt_slash)
    os.makedirs(os.path.dirname(fp_fname), exist_ok = True)
    open(fp_fname, 'w').write(fp_t)
    fp_s = bs4.BeautifulSoup(fp_t, 'html.parser')
    q_s = fp_s.find(text = re.compile('Quote (O|o)f (T|t)he (D|d)ay'))
    if q_s is None:
        open('errors.txt', 'a').write(str(dt) + '\n')
        print('error {}'.format(str(dt)))
        continue
    q_s = q_s.parent
    qtitle = qotd_untitle(q_s)
    while len(q_s.text) < 85 or qtitle == qtitle.title() or q_s.text.endswith(':'):
        q_s = q_s.parent
        qtitle = qotd_untitle(q_s)
    print(q_s)
    isodate = datetime.datetime.strftime(dt, '%Y-%m-%d')
    c.execute('insert or replace into quotes(date, quote) values (?, ?)', (isodate, str(q_s)))
    db.commit()
    print(dt)

c.close()
